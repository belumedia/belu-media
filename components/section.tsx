import React, { Component, createRef } from 'react';
import { slugify } from '../helpers/helper';


interface IProps {
    type: string;
}

export default class Section extends Component<IProps, {}> {

    static defaultProps: {
        background: '#fff';
    };

    private sectionRef = React.createRef<HTMLDivElement>()

    scrollTop: number;
    tweened: number;
    isActive: boolean;
    isTouchDown: boolean;
    y: { start: number; end: number; };
    clientHeight: number;
    animation: any;

    constructor(props: IProps) {
        super(props);
        this.sectionRef = createRef();

        this.scrollTop = 0;
        this.tweened = 0;
        this.isActive = false;
        this.y = {
            start: 0,
            end: 0
        }
        this.animation = null;
        this.onMouseWheel = this.onMouseWheel.bind(this);
        this.onTouchStart = this.onTouchStart.bind(this)
        this.onTouchMove = this.onTouchMove.bind(this)
        this.onTouchEnd = this.onTouchEnd.bind(this)
    }

    onResize() {
        throw new Error('Method not implemented.');
    }

    onMouseWheel = (event: WheelEvent) => {

        if (event.deltaY < 0) {
            this.scrollUp(event, event.deltaY)
        }
        else {
            this.scrollDown(event, event.deltaY);
        }

    }

    onTouchStart = (event: TouchEvent) => {
        this.isTouchDown = true;
        this.y.start = event.touches[0].clientY;
        this.y.end = event.touches[0].clientY;
    }

    onTouchMove = (event: TouchEvent) => {
        if (!this.isTouchDown) return
        this.y.end = event.touches[0].clientY;
    }

    onTouchEnd = (event: Event) => {
        if (!this.isTouchDown) return;

        this.isTouchDown = false;

        if (this.y.start - 25 > this.y.end) {
            this.scrollDown(event, this.y.start - this.y.end)
        } else if (this.y.start + 25 < this.y.end) {
            this.scrollUp(event, this.y.start - this.y.end)
        }
    }


    scrollUp = (event: Event, deltaY: number) => {

        if (this.scrollTop < 0) return;

        event.stopPropagation();
        this.scrollTop += deltaY;

    }

    scrollDown = (event: Event, deltaY: number) => {

        if (this.sectionRef.current.clientHeight - window.innerHeight <= this.scrollTop) return;

        event.stopPropagation();
        this.scrollTop -= deltaY * -1;

    }


    onUpdate = () => {
        if (!this.isActive) return;
        // sets the initial value (no interpolation) - translate the scroll value

        if (Math.abs(this.scrollTop - this.tweened) > 0) {
            // you can change `.072` for the acceleration of scroll
            let top = this.tweened += .072 * (this.scrollTop - this.tweened); // update value of Y translation 

            if (top < 0) {
                top = 0;
            }
            else if (top > this.sectionRef.current.clientHeight) {
                top = this.sectionRef.current.clientHeight;
            }

            this.sectionRef.current.style.transform = `translateY(${(top * -1)}px)`;
        }

        this.animation = window.requestAnimationFrame(this.onUpdate);
    }

    style = () => {
        this.sectionRef.current.style.transform = 'translateY(0)';
    }

    enable() {
        this.isActive = true
        this.onUpdate()
    }

    disable() {
        this.isActive = false
        this.onUpdate()
    }


    render() {

        return (
         
            <div ref={this.sectionRef} className={`section section-${slugify(this.props.type)}`}>
                {this.props.children}
            </div>
       
        );
    }
}