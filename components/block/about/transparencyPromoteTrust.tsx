import React, { useEffect, useRef, useState } from 'react';
import gsap from 'gsap';
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger';

if (typeof window !== 'undefined') {
    gsap.registerPlugin(ScrollTrigger)
}

interface IProps {
    title: string;
    content: string;
}


function TransparencyPromoteTrust({ title, content }: IProps) {

    const sectionInnerRef = useRef<HTMLDivElement | null>(null);
    const highlightRef = useRef<HTMLHeadingElement | null>(null);
    const contentTextRef = useRef<HTMLDivElement | null>(null);
    const backgroundRef = useRef<HTMLImageElement | null>(null);
    const drop1Ref = useRef<HTMLImageElement | null>(null);
    const drop2Ref = useRef<HTMLImageElement | null>(null);
    const drop3Ref = useRef<HTMLImageElement | null>(null);
    const drop4Ref = useRef<HTMLImageElement | null>(null);
    const drop5Ref = useRef<HTMLImageElement | null>(null);
    const [tweenTransparency] = useState(gsap.timeline({ paused: true }))


    useEffect(() => {

        tweenTransparency
            .to([highlightRef.current, contentTextRef.current], {
                y: 0,
                opacity: 1,
                stagger: 0.05,
                scrollTrigger: {
                    trigger: sectionInnerRef.current,
                    start: "top bottom",
                    scrub: 1
                }
            })
            .to(backgroundRef.current, {
                y: 0,
                scrollTrigger: {
                    trigger: sectionInnerRef.current,
                    start: "top bottom",
                    scrub: 1
                }
            })
            .to([drop1Ref.current, drop2Ref.current, drop3Ref.current, drop4Ref.current, drop5Ref.current,], {
                y: "-300px",
                duration: .2,
                stagger: 0.05,
                scrollTrigger: {
                    trigger: sectionInnerRef.current,
                    start: "top bottom",
                    scrub: 1
                }
            })
            .play()

    }, [])

    return (

        <div className="section__inner" ref={sectionInnerRef}>
            <div className="block--content">
                <div className="grid">
                    <div className="grid--wrapper">
                        <h2 className="grid--header__title" ref={highlightRef}>
                            {title}
                        </h2>
                        <div className="grid--content__text" ref={contentTextRef}>
                            {content}
                        </div>
                    </div>
                </div>
            </div>
            <div className="block--images">
                <img src="/img/about/watersplash-background-1.png" className="transparency-bg" ref={backgroundRef} />
                <img src="/img/about/watersplash-drop1.png" className="drop" ref={drop1Ref} />
                <img src="/img/about/watersplash-drop2.png" className="drop" ref={drop2Ref} />
                <img src="/img/about/watersplash-drop3.png" className="drop" ref={drop3Ref} />
                <img src="/img/about/watersplash-drop4.png" className="drop" ref={drop4Ref} />
                <img src="/img/about/watersplash-drop5.png" className="drop" ref={drop5Ref} />
            </div>
        </div>

    );
}

export default TransparencyPromoteTrust;