import React, { forwardRef } from 'react';

interface IProps {
    content: string;
    current: boolean;
}

const Content = ({ content, current } : IProps) => {

    return (
        <div className={`value--content ${current ? 'active' : ''}`}>
            <p>{content}</p>
        </div>
    )
}


export default Content; 