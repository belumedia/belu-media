import React, { forwardRef } from 'react';

interface IProps {
    title: string;
    icon: string;
    background: string;
    content: string;
    idx: number;
    current: number;
    onSetCurrent: (e: any, idx: number) => void
}

const Value = forwardRef<HTMLDivElement, IProps>(
    ({ title, icon, content, idx, current, onSetCurrent }, ref) => {

        let className = 'value';
        if (current === null) className;
        else if (current === idx) className += ' expanded'
        else if (current !== idx) className += ' minified';


        return (
            <div className={className} onClick={(e) => onSetCurrent(e, idx)} ref={ref}>
                <div className="value--icon" dangerouslySetInnerHTML={{ __html: icon }} />
                <div className="value--title">{title}</div>
                <div className="value--content"><p>{content}</p></div>
            </div>
        )
    }
);

export default Value; 