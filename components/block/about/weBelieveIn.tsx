import React, { useEffect, useRef, useState } from 'react';
import gsap from 'gsap';
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger';
import { split } from '@lib/splitText';

if (typeof window !== 'undefined') {
    gsap.registerPlugin(ScrollTrigger)
}

interface IProps {
    title: string;
    subtitle: string;
}


function WeBelieveIn({ title, subtitle }: IProps) {

    const sectionInnerRef = useRef<HTMLDivElement | null>(null);
    const headlineRef = useRef<HTMLHeadingElement | null>(null);
    const contentTextRef = useRef<HTMLDivElement | null>(null);
    const [tweenWhatWeDo] = useState(gsap.timeline({ paused: true }))


    useEffect(() => {

        split({ element: headlineRef.current })
        split({ element: contentTextRef.current })
   
        tweenWhatWeDo
            .to([headlineRef.current.children], {
                y: 0,
                opacity: 1,
                stagger: 0.15,
                scrollTrigger: {
                    trigger: sectionInnerRef.current,
                    start: "top bottom",
                    end: "center center",
                    scrub: 1
                }
            })
            .to([contentTextRef.current], {
                y: 0,
                opacity: .8,
                stagger: 0.15,
                scrollTrigger: {
                    trigger: sectionInnerRef.current,
                    start: "top bottom",
                    end: "center center",
                    scrub: 1
                }
            })
            .play()

    }, [])

    return (

        <div className="section__inner" ref={sectionInnerRef}>
            <div className="block--content">

                <h2 className="block--title" ref={headlineRef}>
                    {title}
                </h2>

                <div className="block--subtitle" ref={contentTextRef}>
                    {subtitle}
                </div>

            </div>

            <img src="/img/about/we-believe-in.png" className="we-believe-in-bg"/>

        </div>

    );
}

export default WeBelieveIn;