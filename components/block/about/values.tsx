import React, { useEffect, useRef, useState } from "react";
import gsap from "gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import Link from "next/link";
import Value from "./values/value";
import Content from "./values/content";
import { useAppContext } from "state/contexts/AppContext";

if (typeof window !== "undefined") {
  gsap.registerPlugin(ScrollTrigger);
}

interface IProps {
  id: string;
  title: string;
  link: {
    text: string;
    page: {
      slug: string;
    };
  };
  values: [
    {
      id: string;
      title: string;
      content: string;
      background: string;
      color: string;
      icon: string;
    }
  ];
}

function Values({ title, link, values }: IProps) {
  const { dispatch } = useAppContext();
  const sectionInnerRef = useRef<HTMLDivElement | null>(null);
  const itemsListRef = useRef<HTMLDivElement | null>(null);
  const headlineRef = useRef<HTMLDivElement | null>(null);
  const linkRef = useRef<HTMLDivElement | null>(null);
  const itemExpanded = useRef<Boolean>(false);
  const itemsListRect = useRef<any | null>(null);
  const xItemsList = useRef<number>(0);
  const [tweenValues] = useState(gsap.timeline({ paused: true }));
  const [activeValue, setActiveValue] = useState<number | null>(null);
  const valuesRef = useRef([]);
  const sliderRef = useRef({
    current: -1,
    last: -1,
    direction: null,
    active: 0,
  });

  useEffect(() => {
    const valuesElemnts = valuesRef.current;
    const itemsList = itemsListRef.current;

    tweenValues
      .to(headlineRef.current, {
        y: 0,
        opacity: 1,
        scrollTrigger: {
          trigger: sectionInnerRef.current,
          endTrigger: headlineRef.current,
          scrub: 1,
        },
      })
      .to(linkRef.current, {
        width: "100%",
        opacity: 1,
        scrollTrigger: {
          trigger: sectionInnerRef.current,
        
          start: "top bottom",
          scrub: 1,
        },
      })
      .play();

    if (window.innerWidth < 768) {
      setActiveValue(sliderRef.current.active);
    }

    valuesElemnts.forEach((value: HTMLDivElement) => {
      value.addEventListener(
        "mouseover",
        (evt: MouseEvent & { currentTarget: HTMLDivElement }) =>
          onMouseOver(evt)
      );
      value.addEventListener(
        "mouseout",
        (evt: MouseEvent & { currentTarget: HTMLDivElement }) => onMouseOut(evt)
      );
    });

    // itemsList.addEventListener(
    //   "touchstart",
    //   (
    //     evt: TouchEvent & {
    //       target: HTMLDivElement;
    //       currentTarget: HTMLDivElement;
    //     }
    //   ) => onTouchStart(evt)
    // );

    // itemsList.addEventListener(
    //   "touchend",
    //   (evt: TouchEvent & { currentTarget: HTMLDivElement }) => onTouchEnd(evt)
    // );

    // itemsList.addEventListener("touchmove", (evt: TouchEvent) =>
    //   onTouchMove(evt)
    // );

    return () => {
      valuesElemnts.forEach((value: HTMLDivElement) => {
        if (!value) return;
        value.removeEventListener("mouseover", onMouseOver);
        value.removeEventListener("mouseout", onMouseOut);
      });
      // itemsList.removeEventListener("touchstart", onTouchStart);
      // itemsList.removeEventListener("touchend", onTouchEnd);
      // itemsList.removeEventListener("touchmove", onTouchMove);
    };
  }, []);

  function onMouseOver(evt: MouseEvent & { currentTarget: HTMLDivElement }) {
    if (evt.currentTarget.classList.contains("expanded")) {
      dispatch({
        type: "CHANGE_CURSOR",
        payload: { style: "cursor--hover-item-close" },
      });
      return;
    }
    dispatch({
      type: "CHANGE_CURSOR",
      payload: { style: "cursor--hover-item-details" },
    });
  }

  function onMouseOut(evt: MouseEvent & { currentTarget: HTMLDivElement }) {
    dispatch({ type: "DEFAULT_CURSOR" });
  }

  function onSetCurrent(
    evt: MouseEvent & { currentTarget: HTMLDivElement },
    selected: number
  ) {
    if (activeValue === selected) {
      setActiveValue(null);
      itemExpanded.current = false;
      gsap.to(itemsListRef.current, { x: "0%", duration: 0.5 });
      gsap.to(sectionInnerRef.current, {
        backgroundColor: "inherit",
        color: "inherit",
        duration: 0.5,
      });
      return;
    }

    setActiveValue(selected);

    const currentItemRect = evt.currentTarget.getBoundingClientRect();
    const { background, color } = values.find((value, idx) => idx === selected);
    const expWidth = currentItemRect.width * 1.25;
    const difference = Math.round(expWidth - currentItemRect.width) / 2;

    if (!itemExpanded.current) {
      itemExpanded.current = true;
      itemsListRect.current = itemsListRef.current.getBoundingClientRect();
      xItemsList.current =
        window.innerWidth / 2 -
        (currentItemRect.left - difference) -
        expWidth / 2;
    } else {
      const offsetX =
        itemsListRef.current.getBoundingClientRect().left -
        itemsListRect.current.left;
      xItemsList.current =
        window.innerWidth / 2 -
        (currentItemRect.left - difference - offsetX) -
        expWidth / 2;
    }

    if (background && color)
      gsap.to(sectionInnerRef.current, {
        backgroundColor: background,
        color,
        duration: 0.5,
      });

    if (xItemsList.current !== 0)
      gsap.to(itemsListRef.current, { x: xItemsList.current, duration: 0.5 });
  }

  function onTouchStart(
    evt: TouchEvent & {
      target: HTMLDivElement;
      currentTarget: HTMLDivElement;
    }
  ) {
    evt.preventDefault();
    if (window.innerWidth < 768) {
      console.log('test')
      evt.stopPropagation();
    }

    itemsListRef.current.classList.add("scrolling");
    sliderRef.current.last = evt.touches[0].clientX;

    gsap.to(itemsListRef.current, {
      scale: 0.8,
      duration: 0.5,
    });

    return false;
  }

  function onTouchEnd(evt: TouchEvent & { currentTarget: HTMLDivElement }) {
    evt.preventDefault();

    if (window.innerWidth < 768) {
      evt.stopPropagation();
    }

    itemsListRef.current.classList.remove("scrolling");

    if (sliderRef.current.direction === "left") {
      if (sliderRef.current.active < valuesRef.current.length - 1) {
        sliderRef.current.active = sliderRef.current.active + 1;
        setActiveValue(() => sliderRef.current.active);
      }
    } else if (sliderRef.current.direction === "right") {
      if (sliderRef.current.active) {
        sliderRef.current.active = sliderRef.current.active - 1;
        setActiveValue(() => sliderRef.current.active);
      }
    }

    gsap.to(itemsListRef.current, {
      scale: 1,
      duration: 0.5,
    });

    return false;
  }

  function onTouchMove(evt: TouchEvent) {
    evt.preventDefault();

    if (window.innerWidth < 768) {
      evt.stopPropagation();
    }

    sliderRef.current.current = evt.touches[0].clientX;

    if (sliderRef.current.last > -1 && sliderRef.current.current > -1) {
      sliderRef.current.direction =
        sliderRef.current.last < sliderRef.current.current ? "right" : "left";
    }
  }

  return (
    <div className="section__inner" ref={sectionInnerRef}>
      <div className="block--content">
        <div className="block--values">
          <div className="block--values__title" ref={headlineRef}>
            <h2>{title}</h2>
          </div>
        
          <div className="block--values__items" ref={itemsListRef}>
            {values.map(({ id, title, icon,content, background }, idx: number) => {
              return (
                <Value
                  title={title}
                  icon={icon}
                  background={background}
                  idx={idx}
                  content={content}
                  current={activeValue}
                  onSetCurrent={onSetCurrent}
                  ref={(el: any) => (valuesRef.current[idx] = el)}
                  key={id}
                />
              );
            })}
          </div>

          <div className="block--values__content">
            {values.map(({ id, content }, idx: number) => (
              <Content
                content={content}
                current={activeValue === idx}
                key={`${id}-content`}
              />
            ))}
          </div>
        </div>

        <div className="block--title" ref={linkRef}>
          <h2>
            <Link href={link?.page?.slug}>
              <a>{link?.text}</a>
            </Link>
          </h2>
        </div>
      </div>
    </div>
  );
}

export default Values;
