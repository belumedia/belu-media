import React, { useEffect, useRef, useState } from "react";
import gsap from "gsap";
import { Converter } from "showdown";
import { split } from "@lib/splitText";

interface Props {
  title: string;
  content: string;
  image: { url: string };
}

export default function TextImage({ title, content, image }: Props) {
  const [tweenTextImage] = useState(gsap.timeline({ paused: true }));
  const sectionInnerRef = useRef<HTMLDivElement | null>(null);
  const titleRef = useRef<HTMLHeadingElement | null>(null);
  const contentRef = useRef<HTMLDivElement | null>(null);
  const convert = new Converter();

  useEffect(() => {
    split({ element: titleRef.current });

    tweenTextImage
      .to([titleRef.current.children, contentRef.current], {
        y: "0%",
        opacity: 1,
        stagger: 0.05,
        scrollTrigger: {
          trigger: sectionInnerRef.current,
          start: "top bottom",
          end: "center bottom",
          scrub: 1,
        },
      })
      .play();
      
  }, []);

  return (
    <div
      className="section__inner"
      style={
        { ["--backgroundImage"]: `url(${image.url})` } as React.CSSProperties
      }
      ref={sectionInnerRef}
    >
      <div className="block--content">
        <h2 className="block--content__title" ref={titleRef}>
          {title}
        </h2>
        <div
          className="block--content__paragraph"
          dangerouslySetInnerHTML={{ __html: convert.makeHtml(content) }}
          ref={contentRef}
        />
      </div>
    </div>
  );
}
