import React, { useEffect, useRef, useState } from 'react';
import gsap from 'gsap';
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger';
import { split } from '@lib/splitText';
import Water from './quote/water';

if (typeof window !== 'undefined') {
    gsap.registerPlugin(ScrollTrigger)
}

interface IProps {
    title: string;
    subtitle: string;
    background: string;
    color: string;
}


function Quote({ title, subtitle, background, color }: IProps) {

    const sectionInnerRef = useRef<HTMLDivElement | null>(null);
    const headlineRef = useRef<HTMLHeadingElement | null>(null);
    const contentTextRef = useRef<HTMLDivElement | null>(null);

    const [tweenQuote] = useState(gsap.timeline({ paused: true }))


    useEffect(() => {

        split({ element: headlineRef.current })

        tweenQuote
            .to([headlineRef.current.children, contentTextRef.current], {
                y: 0,
                opacity: 1,
                stagger: 0.15,
                scrollTrigger: {
                    trigger: sectionInnerRef.current,
                    start: "top bottom",
                    end: "center center",
                    scrub: 1
                }
            })
            .play()

    }, [])



    return (

        <>
            <div className="section__inner" style={{["--bg-color"] : background,["--color"] : color} as React.CSSProperties} ref={sectionInnerRef}>
                <div className="block--content">

                    <h2 className="block--title" ref={headlineRef}>
                        {title}
                    </h2>

                    <div className="block--subtitle" ref={contentTextRef}>
                        {subtitle}
                    </div>

                </div>
            </div>
           <Water/>
        </>

    );
}

export default Quote;