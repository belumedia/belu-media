import { useEffect, useState } from 'react';
import { useInView } from 'react-intersection-observer';

const Form = (props) => {

    const [ref, inView, entry] = useInView({ threshold: 0.5, triggerOnce: true });

    const [values, setValues] = useState({
        name: '',
        email: '',
        phone: '',
        message: ''
    })
    const [loading, setLoading] = useState(false);
    const [errors, setErrors] = useState({});
    const { setSubmitted } = props;

    const API_URL = process.env.NEXT_PUBLIC_API_HOST;
    const EMAIL_TO = process.env.EMAIL_TO;
    const EMAIL_SUBJECT = process.env.EMAIL_SUBJECT;

    useEffect(() => {

        if (inView) {
            const item = entry.target;
            item.classList.add('form-component--animated')
        }

    }, [inView]);

    const constraints = {
        name: {
            presence: true,
        },
        email: {
            presence: true,
            email: true
        },
        phone: {
            presence: true,
            format: {
                pattern: "[0-9]+",
                flags: "i",
                message: "can only contain a-z and 0-9"
            }
        },
        message: {
            presence: true,
        }
    }

    const _onChange = e => {
        const name = e.currentTarget.name;
        const value = e.currentTarget.value;
        setValues({
            ...values,
            [name]: value
        })
    }

    const _onSubmit = async (e) => {
        e.preventDefault();
        const validation = validate(values, constraints);
        setLoading(true);
        if (!validation) {

            const { name, phone, email, message } = values;
            const html = `
                <p><label>Name: </label>${name}</p>
                <p><label>Phone: </label>${phone}</p>
                <p><label>Email: </label>${email}</p>
                <p><label>Message: </label>${message}</p>
            `;

            const body = {
                "to": EMAIL_TO,
                "subject": EMAIL_SUBJECT,
                "text": JSON.stringify(values),
                "html": html
            };

            await fetch(`${API_URL}/email`, {
                method: 'post',
                headers: {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(body)
            }).then((res) => {
                setLoading(false);
                res.status === 200 ? setSubmitted(true) : setSubmitted(false)
            })
        }
        else {
            setLoading(false);
            setErrors(validation);
        }
    }

    return (


        <div className="form-component" ref={ref}>
            <div className="form-component__inner">
                <form className="form" onSubmit={_onSubmit}>
                    <div className={`form--group ${errors.name ? 'has-error' : ''}`}>
                        <input type="text" onChange={_onChange} name="name" value={values.name} placeholder="Name" className="form__control" required />
                    </div>
                    <div className={`form--group ${errors.phone ? 'has-error' : ''}`}>
                        <input type="text" onChange={_onChange} name="phone" value={values.phone} placeholder="Phone" className="form__control" required />
                    </div>
                    <div className={`form--group ${errors.email ? 'has-error' : ''}`}>
                        <input type="email" onChange={_onChange} name="email" value={values.email} placeholder="Email" className="form__control" required />
                    </div>
                    <div className={`form--group ${errors.message ? 'has-error' : ''}`}>
                        <textarea type="text" onChange={_onChange} name="message" value={values.message} placeholder="Message" className="form__control">
                        </textarea>
                    </div>
                    <div className="form--group">
                        <button type="submit" className="btn btn__link btn__arrow icon ion-md-arrow-forward" disabled={loading}>
                            Send <strong> Message </strong>
                        </button>
                    </div>
                </form>
            </div>
        </div>


    );
};

export default Form;