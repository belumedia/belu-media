import React, { useEffect, useRef, useState } from 'react';
import gsap from 'gsap';
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger';
import Icon from './icons/icon';
import { useRouter } from 'next/router';

if (typeof window !== 'undefined') {
  gsap.registerPlugin(ScrollTrigger);
}

interface IProps {
  items: [
    {
      id: string;
      name: string;
      icon: {
        url: string;
      };
    }
  ];
}

const IconsItems = ({ items }: IProps) => {
  const sectionInnerRef = useRef<HTMLDivElement | null>(null);
  const iconsRef = useRef([]);
  const router = useRouter();
  const [tweenQuote] = useState(gsap.timeline({ paused: true }));

  useEffect(() => {
    const iconsList = iconsRef.current;

    tweenQuote
      .to([iconsList], {
        y: 0,
        opacity: 1,
        stagger: 0.1,
        scrollTrigger: {
          trigger: sectionInnerRef.current,
          start: 'top bottom',
          end: 'center center',
          scrub: 1,
        },
      })
      .play();
  }, [router.query.slug]);

  return (
    <div className="section__inner" ref={sectionInnerRef}>
      <div className="block--icons">
        {items.map(({ id, name, icon }, i) => (
          <Icon
            name={name}
            url={icon?.url}
            ref={(el) => (iconsRef.current[i] = el)}
            key={id}
          />
        ))}
      </div>
    </div>
  );
};

export default IconsItems;
