import { useEffect, useState, useRef } from "react";
import { Converter } from "showdown";
import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import { split } from "@lib/splitText";
import Button from "../button";

if (typeof window !== undefined) {
  gsap.registerPlugin(ScrollTrigger);
}

interface IProps {
  title: string;
  subtitle?: string;
  content: string;
  target?: {
    id: string;
    slug: string;
    name: string;
  };
  longText: boolean;
  background: string;
  color: string;
}

const Text = ({
  title,
  subtitle,
  content,
  target,
  longText,
  background,
  color,
}: IProps) => {
  const [textTween] = useState(gsap.timeline({ paused: true }));
  const converter = new Converter();

  const sectionInnerRef = useRef<HTMLDivElement | null>();
  const subtitleRef = useRef(null);
  const paragraphRef = useRef(null);
  const buttonRef = useRef(null);

  useEffect(() => {
    const paragraph = paragraphRef.current.firstChild;

    if (paragraph) {
      split({ element: paragraph });
    }

    if (subtitleRef.current) {
      textTween.to([subtitleRef.current.children], {
        duration: 1,
        autoAlpha: 1,
        y: 0,
        ease: "power3.out",
      });
    }

    textTween.to([paragraph.children], {
      delay: 1,
      duration: 1,
      autoAlpha: 1,
      y: 0,
      ease: "power3.out",
      stagger: {
        amount: 0.5,
      },
      scrollTrigger: {
        trigger: sectionInnerRef.current,
        start: "top center",
        end: "+=100",
        scrub: 1,
      },
    });

    if (buttonRef.current) {
      textTween.to(buttonRef.current.children, {
        duration: 1,
        autoAlpha: 1,
        y: 0,
        ease: "power3.out",
        scrollTrigger: {
          trigger: sectionInnerRef.current,
          start: "top center",
          end: "+=200",
          scrub: 1,
        },
      });
    }

    textTween.play();

    return () => {
      textTween.kill();
    };
  }, []);

  return (
    <div
      className="section__inner"
      style={
        {
          ["--bg-color"]: background,
          ["--color"]: color,
        } as React.CSSProperties
      }
      ref={sectionInnerRef}
    >
      <div className={longText ? "long--text" : "text"}>
        <div className="text__inner">
          <div className="text__columns">
            {subtitle && (
              <div className="text__columns--column">
                <div className="text__subtitle" ref={subtitleRef}>
                  <h3>
                    <span>{subtitle}</span>
                  </h3>
                </div>
              </div>
            )}

            <div className="text__columns--column">
              {title && (
                <div className="text__title">
                  <h1 className="text__title">{title}</h1>
                </div>
              )}
              <div
                className="text__paragraph"
                ref={paragraphRef}
                dangerouslySetInnerHTML={{
                  __html: converter.makeHtml(content),
                }}
              />

              {target && (
                <div className="text__paragraph text__button" ref={buttonRef}>
                  <Button target={target} />
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Text;
