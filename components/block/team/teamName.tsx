import { forwardRef } from 'react';

type Props = {
    name: string;
};

const TeamName = forwardRef<HTMLSpanElement, Props>(({ name }, teamName) => <span className="team__name" ref={teamName}> {name}</span>);

export default TeamName;