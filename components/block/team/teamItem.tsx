import { forwardRef } from 'react';

type Props = {
    photo: any;
    name: string;
    index: number;
}

const TeamItem = forwardRef<HTMLDivElement,Props>(({ photo, name, index }, teamItem) => {

    return (
        <div className="team-item team-item--employee" ref={teamItem}>
            <div className="team-item__inner">

                <div className="team-item__image">
                    <div className="team-item__image-inner">
                        {photo &&
                            <img
                                sizes="(max-width: 600px) 100vw, 600px"
                                srcSet={`${photo.formats.thumbnail.url} 104w, ${photo.formats.small.url} 332w, ${photo.formats.medium.url} 498w, ${photo.url} 600w`}
                                src={photo.url}
                                data-src={photo.url}
                                alt={photo.name}
                            />
                        }
                    </div>
                </div>

                <h2 className="team-item__name">
                    {name}
                </h2>

            </div>
        </div>
    )

})

export default TeamItem;