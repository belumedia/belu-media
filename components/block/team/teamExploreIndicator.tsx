import { forwardRef } from 'react';

type Props = {}

const TeamExploreIndicator = forwardRef<HTMLDivElement, Props>(({}, exploreIndicatorRef) => {

    return ( 
        <>
            <div className="team__feedback"></div>
            <div className="team__explore">
                <div className="team__explore--indicator" ref={exploreIndicatorRef}>
                    <span className="team__explore--circle team__explore--circle--left">
                    </span>
                    <span className="team__explore--line">
                    </span>
                    <span className="team__explore--circle team__explore--circle--right">
                    </span>
                    <span className="team__explore--label">
                        Hold and drag to explore
                    </span>
                </div>
            </div>
        </>
    );
});


export default TeamExploreIndicator;