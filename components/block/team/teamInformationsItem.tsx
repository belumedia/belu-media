type Props = {
    name: string;
    role: string; 
    description: string;
}

const TeamInformationsItem = ({ name, role, description } : Props) => {

    return (
        <div className="team__informations--single">
            <div className="team__informations--single--header">
                <h2 className="team__informations--single--name">
                    {name}
                </h2>
                <p className="team__informations--single--role">
                    {role}
                </p>
            </div>
            <div className="team__informations--single--description">
                {description}
            </div>
        </div>
    );
};

export default TeamInformationsItem;