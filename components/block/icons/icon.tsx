import React, { forwardRef } from "react";

interface Props {
  name: string;
  description?: string;
  url: string;
}

const Icon = forwardRef<HTMLDivElement, Props>(
  ({ name, description, url }, ref) => {
    return (
      <div className="block--icons__icon" ref={ref}>
        {url && (
          <div className="block--icons__icon--img">
            <img src={url} />
          </div>
        )}
        {name && <div className="block--icons__icon--title">{name}</div>}
        {description && (
          <div className="block--icons__icon--description">{description}</div>
        )}
      </div>
    );
  }
);

export default Icon;
