import React from "react";
import CaseStudy from "./case-studies/caseStudy";

interface Props {
  items: {
    id: string;
    name: string;
    campaign: string;
    background: string;
    trackableLink: string;
    preview: {
      url: string;
      width: number;
      height: number;
    };
  }[];
}

const CaseStudies = ({ items }: Props) => {
  return (
    <div className="section__inner">
      <div className="block--content">
        <div className="case-studies__list">
          {items.map(
            ({ id, name, campaign, preview, background, trackableLink }) => (
              <CaseStudy
                name={name}
                campaign={campaign}
                background={background}
                preview={preview}
                trackableLink={trackableLink}
                key={id}
              />
            )
          )}
        </div>
      </div>
    </div>
  );
};

export default CaseStudies;
