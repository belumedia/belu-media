import React, { useEffect, useRef, useState } from "react";
import gsap from "gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import { split } from "@lib/splitText";

if (typeof window !== "undefined") {
  gsap.registerPlugin(ScrollTrigger);
}

interface Props {
  title: string;
  subtitle: string;
  content: string;
  image: {
    url: string;
  };
}

const Title = ({ title, subtitle, content, image }: Props) => {
  const titleRef = useRef<HTMLDivElement | null>(null);
  const highlightRef = useRef<HTMLParagraphElement | null>(null);
  const headerTextRef = useRef<HTMLParagraphElement | null>(null);
  const contentRef = useRef<HTMLParagraphElement | null>(null);
  const [tweenTitle] = useState(gsap.timeline({ paused: true }));

  useEffect(() => {
    split({ element: headerTextRef.current });
    split({ element: highlightRef.current });
    split({ element: contentRef.current });

    tweenTitle
      .to(
        [
          highlightRef.current.children,
          headerTextRef.current.children,
          contentRef.current.children,
        ],
        {
          y: "50%",
          opacity: 0,
          stagger: 0.05,
          scrollTrigger: {
            trigger: titleRef.current,
            start: "top top",
            scrub: 1,
          },
        }
      )
      .play();
  }, []);

  return (
    <div
      className="section__inner"
      style={
        { ["--backgroundImage"]: `url(${image?.url})` } as React.CSSProperties
      }
    >
      <div className="block--content">
        <div className="title" ref={titleRef}>
          <div className="title--wrapper">
            <h1 className="title--header__title">
              <p className="title--header__text" ref={headerTextRef}>
                {subtitle}
              </p>
              <p className="title--header__highlight" ref={highlightRef}>
                {title}
              </p>
            </h1>
            <div className="title--content">
              <p ref={contentRef}>{content}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Title;
