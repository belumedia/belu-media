import Link from 'next/link';
import React from 'react';

const TabItem = ({ children, title, href }) => {
    return (
        <>
            <Link href={href || "/"}>
                <a>
                    {title}
                </a>
            </Link>
            {children}
        </>
    );
};

export default TabItem;