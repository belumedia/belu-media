import React from "react";
import { Converter } from "showdown";

interface IProps {
  title: string;
  content: string;
  image: { url: string };
}

export default function TextImage({ title, content, image }: IProps) {
  const convert = new Converter();

  return (
    <div
      className="image-overlay"
      style={{ backgroundImage: `url(${image.url})` }}
    >
      <div className="section__inner m-auto">
        <div className="flex">
          <div className="text-square">
            <div>
              <h2>{title}</h2>
            </div>
            <div
              dangerouslySetInnerHTML={{ __html: convert.makeHtml(content) }}
            />
          </div>
        </div>
      </div>
    </div>
  );
}
