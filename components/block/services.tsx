import React, { useEffect, useRef, useState } from "react";
import request from "graphql-request";
import gsap from "gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import { split } from "@lib/splitText";
import Icon from "./icons/icon";
import { queryServices } from "graphql/queries";
import Errors from "@components/errors";
import Loading from "@components/loading";
import useSWR from "swr";

if (typeof window !== "undefined") {
  gsap.registerPlugin(ScrollTrigger);
}

interface IProps {
  title: string;
  items: [
    {
      name: string;
      icon: {
        url: string;
      };
    }
  ];
}

const Services = ({ title, items }: IProps) => {
  const { data, error } = useSWR(queryServices, (query) =>
    request(process.env.NEXT_PUBLIC_API_GRAPHQL_HOST, query)
  );

  const sectionInnerRef = useRef<HTMLDivElement | null>(null);
  const headlineRef = useRef<HTMLHeadingElement | null>(null);
  const iconsRef = useRef([]);
  const [tweenQuote] = useState(gsap.timeline({ paused: true }));

  useEffect(() => {
    //split({ element: headlineRef.current });

    const iconsList = iconsRef.current;
    if (headlineRef.current) {
      tweenQuote
        .to([headlineRef.current, iconsList], {
          y: 0,
          opacity: 1,
          stagger: 0.15,
          scrollTrigger: {
            trigger: sectionInnerRef.current,
            start: "top bottom",
            end: "center center",
            scrub: 1,
          },
        })
        .play();
    }
  }, []);

  if (error) return <Errors message={"Error to loading"} />;
  if (!data) return <Loading />;


  return (
    <>
      <div className="section__inner" ref={sectionInnerRef}>
        <div className="block--content">
          <h2 className="block--title" ref={headlineRef}>
            {title}
          </h2>
        </div>

        <div className="block--icons">
          {/* {items.map(({ name, icon: { url } }, i) => <Icon name={name} url={url} key={i} ref={el => iconsRef.current[i] = el} />)} */}
        </div>
      </div>
    </>
  );
};

export default Services;
