import React, { useEffect, useRef, useState } from "react";
import gsap from "gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import Icon from "./icons/icon";
import { useRouter } from "next/router";
import { Converter } from "showdown";
import { split } from "@lib/splitText";

if (typeof window !== "undefined") {
  gsap.registerPlugin(ScrollTrigger);
}

interface Props {
  title: string;
  content: string;
  items: [
    {
      id: string;
      name: string;
      description: string;
      image: {
        url: string;
      };
    }
  ];
}

const IconsTextItems = ({ title, content, items }: Props) => {
  const [tweenQuote] = useState(gsap.timeline({ paused: true }));
  const sectionInnerRef = useRef<HTMLDivElement | null>(null);
  const titleRef = useRef<HTMLHeadingElement | null>(null);
  const contentRef = useRef<HTMLDivElement | null>(null);

  const converter = new Converter();
  const iconsRef = useRef([]);
  const router = useRouter();

  useEffect(() => {
    split({ element: titleRef.current });
    const iconsList = iconsRef.current;

    tweenQuote
      .to([titleRef.current.children, contentRef.current], {
        y: "0%",
        opacity: 1,
        stagger: 0.05,
        scrollTrigger: {
          trigger: sectionInnerRef.current,
          start: "top bottom",
          end: "center bottom",
          scrub: 1,
        },
      })
      .to([iconsList], {
        y: 0,
        opacity: 1,
        stagger: 0.1,
        scrollTrigger: {
          trigger: sectionInnerRef.current,
          start: "top bottom",
          end: "+=75%",
          scrub: 1,
        },
      })
      .play();
  }, [router.query.slug]);

  return (
    <div className="section__inner" ref={sectionInnerRef}>
      <div className="block--content">
        <div className="block--content__title">
          <h2 ref={titleRef}>{title}</h2>
        </div>

        <div
          className="block--content__paragraph"
          dangerouslySetInnerHTML={{ __html: converter.makeHtml(content) }}
          ref={contentRef}
        />

        <div className="block--icons">
          {items?.map(({ name, description, image }, i) => (
            <Icon
              name={name}
              description={description}
              url={image?.url}
              ref={(el) => (iconsRef.current[i] = el)}
              key={name}
            />
          ))}
        </div>
      </div>
    </div>
  );
};

export default IconsTextItems;
