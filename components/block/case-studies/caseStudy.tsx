import React, { useEffect, useRef } from "react";
import Image from "next/image";

interface Props {
  name: string;
  campaign: string;
  background: string;
  trackableLink: string;
  preview: {
    url: string;
    width: number;
    height: number;
  };
}

const CaseStudy = ({
  name,
  campaign,
  background,
  preview,
  trackableLink,
}: Props) => {
  return (
    <div className="case-studies__list--item">
      <a className="case-study" href={trackableLink} target="_new" download>
        <div
          className="case-study__background"
          style={{ ["--background"]: background } as React.CSSProperties}
        >
          <div className="case-study__preview">
            <Image
              src={preview.url}
              width={preview.width}
              height={preview.height}
            />
          </div>
        </div>
        <h3 className="case-study__campaign">{campaign}</h3>
        <h4 className="case-study__name">{name}</h4>
      </a>
    </div>
  );
};

export default CaseStudy;
