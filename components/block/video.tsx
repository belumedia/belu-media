import { useState, useEffect, useRef, Dispatch } from "react";
import { useInView } from "react-intersection-observer";
import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import { useAppContext } from "state/contexts/AppContext";
import { Action, DefaultCursorAction } from "../../state/actions";
import { changeCursor, defaultCursor } from "../../state/action-creators";

if (typeof window !== undefined) {
  gsap.registerPlugin(ScrollTrigger);
}

interface IProps {
  media: {
    mime: string;
    url: string;
    id: string;
  };
}

const Video = ({ media }: IProps) => {
  const [ref, inView] = useInView({ threshold: 0.25, initialInView: true });
  const { dispatch } = useAppContext();
  const blockVideoRef = useRef<HTMLDivElement | null>(null);
  const videoRef = useRef<HTMLVideoElement | null>(null);
  const videoHasPlayed = useRef<boolean>(false);
  const [tweenBlockVideo] = useState(gsap.timeline({ paused: true }));

  useEffect(() => {
    let video = videoRef.current;
    video.addEventListener("mouseenter", onMouseEnter);
    video.addEventListener("mouseleave", onMouseLeave);

    tweenBlockVideo
      .to(blockVideoRef.current, {
        autoAlpha: 1,
        duration: 0.5,
      })
      .to(blockVideoRef.current, {
        scale: 1.5,
        scrollTrigger: {
          trigger: video,
          start: "center center",
          end: "bottom",
          scrub: 1,
        },
      })
      .play();

    return () => {
      dispatch(defaultCursor());

      video.removeEventListener("mouseenter", onMouseEnter);
      video.removeEventListener("mouseleave", onMouseLeave);
    };
  }, []);

  useEffect(() => {
    if (!inView && videoHasPlayed.current) {
      videoRef.current.pause();
    }
  }, [inView]);

  //https://webkit.org/blog/6784/new-video-policies-for-ios/

  const playOrPauseVideo = () => {
    console.log("play");

    if (!videoHasPlayed.current) {
      videoHasPlayed.current = true;
      videoRef.current.currentTime = 0;
      videoRef.current.muted = false;
      videoRef.current.volume = 0.5;
      videoRef.current.play();
      dispatch(changeCursor("cursor--hover-video-pause"));
    } else if (!videoRef?.current.paused && videoHasPlayed.current) {
      videoRef.current.pause();
      dispatch(changeCursor("cursor--hover-video-play"));
    } else if (videoRef?.current.paused && videoHasPlayed.current) {
      videoRef.current.play();
      dispatch(changeCursor("cursor--hover-video-pause"));
    }
  };

  const onMouseEnter = () => {
    if (!videoHasPlayed.current) {
      dispatch(changeCursor("cursor--hover-video-play"));
    } else if (videoRef?.current.paused) {
      dispatch(changeCursor("cursor--hover-video-play"));
    } else if (!videoRef?.current.paused) {
      dispatch(changeCursor("cursor--hover-video-pause"));
    }
  };

  const onMouseLeave = () => {
    dispatch(defaultCursor());
  };

  return (
    <div className="section__inner" ref={ref}>
      <div className="block--video" ref={blockVideoRef}>
        <video
          className="video-media"
          controls={false}
          preload="metadata"
          muted
          autoPlay
          playsInline
          onClick={playOrPauseVideo}
          ref={videoRef}
        >
          <source src={`${media.url}#t=0`} type={media.mime}></source>
        </video>
      </div>
    </div>
  );
};

export default Video;
