import { useState, useEffect, useRef } from 'react';
import Image from 'next/image';
import gsap from 'gsap';
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger';

if (typeof window !== undefined) {
  gsap.registerPlugin(ScrollTrigger);
}

const Clients = ({ title, items }) => {
  const [tweenClients] = useState(gsap.timeline({ paused: true }));
  const logosRef = [];

  const sectionRef = useRef<HTMLHeadingElement | null>(null);
  const titleRef = useRef<HTMLHeadingElement | null>(null);

  useEffect(() => {
    const animationObj = {
      y: 0,
      autoAlpha: 1,
      duration: 1,
    };

    tweenClients
      .to(titleRef.current, {
        scrollTrigger: {
          trigger: sectionRef.current,
          start: 'top center',
          end: '+=200',
          scrub: 1,
        },
        ...animationObj,
      })
      .to(logosRef, {
        stagger: 0.15,
        scrollTrigger: {
          trigger: sectionRef.current,
          start: 'top center',
          end: '+=200',
          scrub: 1,
        },
        ...animationObj,
      })
      .play();
  }, []);

  return (
    <div className="section__inner" ref={sectionRef}>
      <div className="block--logos block--content">
        <div className="block--logos__title" ref={titleRef}>
          <h2>{title}</h2>
        </div>
        {items.map(({ id, name, logo }, idx: number) => (
          <div className="logo" key={id} ref={(el) => (logosRef[idx] = el)}>
            {logo && (
              <Image
                src={logo?.formats?.thumbnail?.url}
                alt={name}
                height={150}
                width={150}
              />
            )}
          </div>
        ))}
      </div>
    </div>
  );
};

export default Clients;
