import Form from "./contacts/form";

type Props = {
    widgets: any
}

const Contacts = ({ widgets } : Props) => {

    return (
        <>
            <Form widgets={widgets}/>
        </>
    );
};

export default Contacts;