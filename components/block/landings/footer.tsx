import { Converter } from "showdown";


interface IProps {
  items: {
    id: string;
    title: string;
    content: string;
  }[];
}

export default function Footer({ items }: IProps) {

  const converter = new Converter();
  return (
    <div className="section__inner">
      <div className="footer">
        {items &&
          items.map(({ title, content }) => (
            <div key={title}>
              <h4 className="footer__items--title">{title}</h4>
              <div
                className="footer__items--content"
                dangerouslySetInnerHTML={{
                  __html: converter.makeHtml(content),
                }}
              />
            </div>
          ))}
      </div>
    </div>
  );
}
