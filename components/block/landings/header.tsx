import React from "react";
interface IProps {
  logo: {
    id: string;
    url: string;
  };
  link: string;
}

export default function Header({ logo, link }: IProps) {
  return (
    <div className="section__inner">
      <a href={link} target="_blank">
        <img src={logo.url} alt="Top Business Tech logo" />
      </a>
    </div>
  );
}
