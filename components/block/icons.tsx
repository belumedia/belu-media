import React, { useEffect, useRef, useState } from 'react';
import gsap from 'gsap';
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger';
import { split } from '@lib/splitText';
import Icon from './icons/icon';
import { useRouter } from 'next/router';
import TabItem from './services/tabItem';

if (typeof window !== 'undefined') {
    gsap.registerPlugin(ScrollTrigger)
}

interface IProps {
    title: string;
    items: [{
        id: string;
        name: string;
        icon: {
            url: string;
        };
    }]
    services: [
        {
            id: string;
            title: string;
            path: string;
        }
    ]
}


const Icons = ({ title, items, services }: IProps) => {

    const sectionInnerRef = useRef<HTMLDivElement | null>(null);
    const headlineRef = useRef<HTMLHeadingElement | null>(null);
    const linksRef = useRef<HTMLDivElement | null>(null);
    const iconsRef = useRef([]);
    const router = useRouter();
    const [tweenQuote] = useState(gsap.timeline({ paused: true }))


    useEffect(() => {

        split({ element: headlineRef.current });

    }, [])

    useEffect(() => {

        const iconsList = iconsRef.current;

        tweenQuote
            .to([headlineRef.current.children, iconsList], {
                y: 0,
                opacity: 1,
                stagger: 0.15,
                scrollTrigger: {
                    trigger: sectionInnerRef.current,
                    start: "top bottom",
                    end: "center center",
                    scrub: 1
                }
            })
            .to(linksRef.current, {
                y: 0,
                opacity: 1,
                scrollTrigger: {
                    trigger: sectionInnerRef.current,
                    start: "bottom bottom",
                    end: "bottom bottom",
                    scrub: 1
                }
            })
            .play()

    }, [router.query.slug])


    return (

        <div className="section__inner" ref={sectionInnerRef}>
            <div className="block--content">
                <h2 className="block--title" ref={headlineRef}>
                    {title}
                </h2>
            </div>

            <div className="block--icons">
                {items.map(({ id, name, icon }, i) => <Icon name={name} url={icon?.url} ref={el => iconsRef.current[i] = el} key={id} />)}
            </div>

            <div className="block--links" ref={linksRef}>
                {services.map(({ id, title, path }) => {
                    return(
                        <div className="block--links__label" key={id}> 
                            <TabItem title={title} href={path}>
                                <svg width="41" height="16" viewBox="0 0 41 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M40.7071 8.70711C41.0976 8.31658 41.0976 7.68342 40.7071 7.29289L34.3431 0.928932C33.9526 0.538408 33.3195 0.538408 32.9289 0.928932C32.5384 1.31946 32.5384 1.95262 32.9289 2.34315L38.5858 8L32.9289 13.6569C32.5384 14.0474 32.5384 14.6805 32.9289 15.0711C33.3195 15.4616 33.9526 15.4616 34.3431 15.0711L40.7071 8.70711ZM0 9L16.0714 9V7L0 7L0 9ZM16.0714 9L40 9V7L16.0714 7V9Z" fill="currentColor" />
                                </svg>
                            </TabItem>
                        </div>
                    ) 
                })}
            </div>

        </div>

    );
}

export default Icons;