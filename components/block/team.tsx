import TeamIntroduction from './team/teamIntroduction';
import TeamCarousel from './team/teamCarousel';

interface IProps {
    title: string;
    content: string;
    people: [{
        id: string;
        name: string;
        photo: any;
    }]
}

const Team = ({ people, title, content }: IProps) => {

    return (
        <div className="section__inner">
            <div className="team">
                <TeamIntroduction title={title} content={content} />
                <TeamCarousel items={people} />
            </div>
        </div>
    )

}

export default Team;