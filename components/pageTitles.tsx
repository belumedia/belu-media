import React from 'react';

const PageTitles = ({ children }) => {
    return (
        children && (
            <div className="section-titles">
                {React.Children.map(children, child => (
                    React.cloneElement(child, {style: {...child.props.style, opacity: 0.5}})
                ))}
            </div>
        )
    );
};

export default PageTitles;