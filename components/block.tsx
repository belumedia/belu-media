import Text from './block/text';
import Video from './block/video';
import Clients from './block/clients';
import Team from './block/team';
import Locations from './block/locations';
import Title from './block/title';
import TransparencyPromoteTrust from './block/about/transparencyPromoteTrust';
import Logo from './block/logo';
import Quote from './block/quote';
import WeBelieveIn from './block/about/weBelieveIn';
import Values from './block/about/values';
import Icons from './block/icons';
import IconsItems from './block/iconsItems';
import TextImage from './block/textImage';
import IconsTextItems from './block/iconsTextItems';
import Header from './block/landings/header';
import Footer from './block/landings/footer';
import CaseStudies from './block/caseStudies';
import ImageText from './block/imageText';

const Block = (props: any) => {
  return (
    <>
      {
        {
          ComponentBlocksBlockText: <Text {...props} />,
          ComponentBlocksBlockVideo: <Video {...props} />,
          ComponentBlocksBlockClients: <Clients {...props} />,
          ComponentBlocksBlockTeam: <Team {...props} />,
          ComponentBlocksBlockLocations: <Locations {...props} />,
          ComponentBlocksBlockTitle: <Title {...props} />,
          ComponentBlocksBlockLogo: <Logo {...props} />,
          ComponentAboutBlockTransparency: (
            <TransparencyPromoteTrust {...props} />
          ),
          ComponentBlocksBlockQuote: <Quote {...props} />,
          ComponentAboutBlockWeBeliveIn: <WeBelieveIn {...props} />,
          ComponentAboutBlockValues: <Values {...props} />,
          ComponentBlocksBlockIcons: <Icons {...props} />,
          ComponentBlocksBlockIconsItems: <IconsItems {...props} />,
          ComponentLandingBlockHeader: <Header {...props} />,
          ComponentLandingBlockFooter: <Footer {...props} />,
          ComponentBlocksBlockTextIcons: <IconsTextItems {...props} />,
          ComponentBlocksBlockImageText: <ImageText {...props} />,
          ComponentBlocksBlockTextImage: <TextImage {...props} />,
          ComponentBlocksBlockCaseStudies: <CaseStudies {...props} />,
        }[props.type]
      }
    </>
  );
};

export default Block;
