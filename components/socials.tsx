import { useEffect, useState } from 'react';
import useSWR from 'swr';
import { request } from 'graphql-request';
import { Converter } from 'showdown';
import { useAppContext } from 'state/contexts/AppContext';
import { querySocials } from "../graphql/queries";
import Loading from '@components/loading';

const Socials = () => {

  const { dispatch } = useAppContext();
  const converter = new Converter;
  const { data, error } = useSWR(querySocials, query => request(process.env.NEXT_PUBLIC_API_GRAPHQL_HOST, query, { slug: 'socials' }))


  if (error) return <div>Error loading widgets </div>
  if (!data) return <Loading />

  const { widgets } = data;
  const { content } = widgets || [];

  return (
    <div className="page-icons-fixed">
      <ul>
        {content && content.map(({ content }, idx: number) => <li dangerouslySetInnerHTML={{ __html: converter.makeHtml(content) }} key={idx} />)}
      </ul>
    </div>
  )

}


export default Socials;