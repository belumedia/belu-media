import React from "react";
import Button from "./button";

interface IProps {
  message: string;
  backToHome?: boolean;
}

const Errors = ({ message, backToHome }: IProps) => {
  return (
    <div className="error">
      <p className="error-message">{message}</p>
      {backToHome && (
        <Button
          target={{
            id: "button-home",
            slug: "/",
            name: "Back to Home",
          }}
        />
      )}
    </div>
  );
};

export default Errors;
