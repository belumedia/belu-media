import { useAppContext } from "state/contexts/AppContext";

interface IProps {
    children: React.ReactChildren
}

const Wrapper = ({ children }: IProps) => {

    const { navigation: { navOpen } } = useAppContext();

    return (
        <div className={`page-canvas ${navOpen ? 'nav-open' : 'nav-close'}`}>
            {children}
        </div>
    )
}

export default Wrapper;