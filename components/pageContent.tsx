import React, { Component, createRef } from "react";
import ReactDOM from "react-dom";
import { gsap } from "gsap";
import { NextRouter, withRouter } from "next/router";
import PageNavigation from "./pageNavigation";
import PageFooter from "@containers/pageFooter";
import Section from "./section";
import Block from "./block";
import { AppContext } from "state/contexts/AppContext";

type Props = {
  router: NextRouter;
  template: string;
  children: React.ReactNodeArray | null;
};

class PageContent extends Component<any, any> {
  pageContentRef = createRef<HTMLDivElement | null>();
  pageNavigationRef = createRef<HTMLDivElement | null>();
  pageFooterRef = createRef<HTMLDivElement | null>();
  sectionContainerRef = createRef<HTMLDivElement | null>();
  sections = [];
  observer: any;
  scrollConfig: {
    ease: number;
    current: number;
    previous: number;
    rounded: number;
  };
  animationFrame: number;
  scrollY: number;
  smoothScrolling: boolean;

  static contextType = AppContext;

  constructor(props: Props) {
    super(props);

    this.state = {
      pageContent: null,
      pageNavigation: null,
      sectionContainer: null,
      pageFooter: null,
      sections: [],
      isCurrentFooter: false,
    };

    this.scrollConfig = {
      ease: 0.1,
      current: 0,
      previous: 0,
      rounded: 0,
    };

    this.smoothScrolling = false;

    this.scrollY = 0;
    this.sections = [];
  }

  componentDidMount() {
    gsap.set(this.pageContentRef.current, { visibility: "visible" });
    gsap.set(this.pageNavigationRef.current, { color: "rgba(255,255,255,1)" });

    //this.onObserverSections();
    this.enableSmoothScrolling();

    document.addEventListener("resize", this.onWindowResize);
    window.addEventListener("scroll", this.onWindowScroll);
  }

  componentWillUnmount() {
    document.removeEventListener("resize", this.onWindowResize);
    window.addEventListener("scroll", this.onWindowScroll);
    cancelAnimationFrame(this.animationFrame);
  }

  componentDidUpdate(prevProps: Props) {
    if (prevProps.router.asPath !== this.props.router.asPath) {
      // this.onObserverSections();
      // this.smoothScrolling();
    }
  }

  shouldComponentUpdate(nextProps: { router: { asPath: any } }) {
    if (nextProps.router.asPath !== this.props.router.asPath) {
      return true;
    }
    return false;
  }

  onScroll = () => {
    this.scrollConfig.current = window.scrollY;
    this.scrollConfig.previous +=
      (this.scrollConfig.current - this.scrollConfig.previous) *
      this.scrollConfig.ease;
    this.scrollConfig.rounded =
      Math.round(this.scrollConfig.previous * 100) / 100;

    if (this.sectionContainerRef.current) {
      this.sectionContainerRef.current.style.transform = `translate3d(0,-${this.scrollConfig.rounded}px, 0)`;
      this.animationFrame = requestAnimationFrame(this.onScroll);
    }
  };

  onWindowScroll = () => {
    this.scrollY = window.scrollY;
  };

  onWindowResize = () => {
    this.enableSmoothScrolling();
  };

  onObserverSections = () => {
    const callback = (entries) => {
      entries.forEach((entry) => {
        if (entry.intersectionRatio > 0.55 && entry.isIntersecting) {
          this.sections.forEach((section) => {
            if (
              section?.sectionRef &&
              ReactDOM.findDOMNode(section.sectionRef.current) ===
                ReactDOM.findDOMNode(entry.target)
            ) {
              if (section?.props?.color)
                gsap.set(this.pageNavigationRef.current, {
                  color: section.props.color,
                });
            }
          });
        }
      });
    };

    this.observer = new IntersectionObserver(callback, {
      root: null,
      threshold: 0.55,
    });

    this.sections.forEach((section) => {
      if (section?.sectionRef)
        this.observer.observe(section.sectionRef.current);
    });
  };

  enableSmoothScrolling = () => {
    if (!this.smoothScrolling) return;

    if (this.sectionContainerRef.current) {
      this.pageContentRef.current.style.position = "fixed";
      this.pageContentRef.current.style.top = "0px";
      this.pageContentRef.current.style.left = "0px";
      this.pageContentRef.current.style.overflow = "hidden";
      document.body.style.height = `${
        this.sectionContainerRef.current.getBoundingClientRect().height
      }px`;
      this.onScroll();
    }
  };

  render() {
    const { sections } = this.props;

    return (
      <>
        <div className="page-navigation" ref={this.pageNavigationRef}>
          <PageNavigation />
        </div>

        <div className="page-content" id={this.props.template} ref={this.pageContentRef}>
          <div className="section-container" ref={this.sectionContainerRef}>
            {sections.map((section: any, idx: number) => (
              <Section
                {...section}
                type={section.type}
                key={`${section.type}-${idx}`}
                ref={(el) => (this.sections[idx] = el)}
              >
                <Block {...section} type={section.type} />
              </Section>
            ))}
            <div className="section-footer" ref={this.pageFooterRef}>
              <PageFooter />
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default withRouter(PageContent);
