import { useEffect, useRef } from "react";

const ScrollDown = () => {

    const pageScrollerRef = useRef<HTMLDivElement | null>(null);

    useEffect(() => {

        const pageScroller = pageScrollerRef.current;

        const onScroll = () => {
            if (window.scrollY + window.innerHeight === document.body.scrollHeight && pageScroller) {
                pageScroller.style.display = 'none';
            }
            else {
                pageScroller.style.display = 'block';
            }
        }

        window.addEventListener("scroll", onScroll);

        return () => {
            window.removeEventListener("scroll", onScroll);
        }

    }, [])

    return (

        <div className="page-scroller" ref={pageScrollerRef}>

            <svg width="9" height="92" viewBox="0 0 9 92" fill="none">
                <path d="M8.29688 0L8.29688 90.8914M8.4137 91L1 80.9365" />
            </svg>

        </div>

    )


}



export default ScrollDown;