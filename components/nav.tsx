import React, { useRef, useState, useEffect } from "react";
import ReactDOM from "react-dom";
import { withRouter, NextRouter } from "next/router";
import { gsap } from "gsap";
import { Converter } from "showdown";
import Logo from "./logo";
import ButtonCloseMenu from "./menu/buttonCloseMenu";
import { useAppContext } from "state/contexts/AppContext";
import Item from "./menu/item";

type NavProps = {
  router: NextRouter;
  items: [
    {
      label: string;
      page?: {
        slug: string;
        title: string;
      };
      landing?: {
        slug: string;
        title: string;
      };
      children: [
        {
          label: string;
          description: string;
          page: {
            slug: string;
          };
        }
      ];
    }
  ];
  widgets: [
    {
      id: string;
      title: string;
      content: string;
    }
  ];
};

const Nav = ({ router, items, widgets }: NavProps) => {
  let menuList = [];
  const navRef = useRef<HTMLDivElement | null>(null);
  const navigationDetailsRef = useRef<HTMLDivElement | null>(null);
  const wrapperInnerRef = useRef<HTMLDivElement | null>(null);
  const blindsRef = useRef<HTMLDivElement | null>(null);
  const toggleSubMenu = useRef<boolean | null>(false);
  const [subMenuIdx, setSubMenuIdx] = useState(null);

  const [menuTween] = useState(gsap.timeline({ paused: true }));
  const [subMenuTween] = useState(gsap.timeline({ paused: true }));
  const {
    navigation: { navOpen },
    dispatch,
  } = useAppContext();
  const converter = new Converter();

  useEffect(() => {
    router.events.on("routeChangeStart", handleRouteChangeStart);
    router.events.on("routeChangeComplete", handleRouteChangeCompleted);

    return () => {
      router.events.off("routeChangeStart", handleRouteChangeStart);
      router.events.off("routeChangeComplete", handleRouteChangeCompleted);
    };
  }, []);

  useEffect(() => {
    if (navOpen) {
      _onMenuOpen();
    } else {
      _onMenuClose();
    }
  }, [navOpen]);

  const _onMenuOpen = () => {
    const navigation_items = menuList;
    const navigation_inner = wrapperInnerRef.current;
    const navigation_blinds = blindsRef.current.children;
    const navigation_details = navigationDetailsRef.current.children;

    menuTween
      .set([navRef.current, navigation_inner], {
        autoAlpha: 1,
      })
      .to(navigation_blinds, {
        delay: 0.5,
        duration: 0.5,
        height: "100%",
        ease: "power3.easeOut",
        stagger: 0.07,
      })
      .to([navigation_items, navigation_details], {
        y: "0%",
        autoAlpha: 1,
        ease: "power3.easeOut",
        duration: 0.3,
        stagger: 0.05,
      })

      .play();
  };

  const _onMenuClose = () => {
    const navigation_items = menuList;
    const navigation_inner = wrapperInnerRef.current;
    const navigation_blinds = blindsRef.current.children;
    const navigation_details = navigationDetailsRef.current.children;

    if (toggleSubMenu.current) {
      const itemToToggle = menuList[subMenuIdx];
      const itemSiblings = menuList.filter((value, i) => subMenuIdx !== i);
      toggleSubMenu.current = false;
      setSubMenuIdx(null);

      menuTween
        .set(itemToToggle.children[1], {
          height: 0,
          autoAlpha: 0,
        })
        .set(itemSiblings, {
          height: "auto",
          autoAlpha: 1,
          y: "0%",
        })
        .play();
    }

    menuTween
      .to([navigation_details, navigation_items], {
        y: "100%",
        autoAlpha: 0,
        ease: "power3.easeOut",
        duration: 0.3,
        stagger: 0.05,
      })
      .to(navigation_blinds, {
        delay: 0.5,
        duration: 0.5,
        height: "0%",
        ease: "power3.easeOut",
        stagger: 0.07,
      })
      .set([navRef.current, navigation_inner], {
        autoAlpha: 0,
      })
      .play();
  };

  const _onToggleSubMenu = (idx: number) => {
    if (!idx) return;

    const itemToToggle = menuList[idx];
    const itemSiblings = menuList.filter(
      (value) =>
        ReactDOM.findDOMNode(value) !== ReactDOM.findDOMNode(itemToToggle)
    );

    // show sub menu
    if (!toggleSubMenu.current) {
      setSubMenuIdx(idx);
      toggleSubMenu.current = true;

      subMenuTween
        .to(itemSiblings, {
          height: 0,
          autoAlpha: 0,
          y: "-100%",
          duration: 0.25,
          stagger: 0.05,
        })
        .to(
          itemToToggle.children[1],
          {
            height: "auto",
            autoAlpha: 1,
            duration: 0.25,
          },
          "+= .2"
        )
        .play();
    } else {
      setSubMenuIdx(false);
      toggleSubMenu.current = false;

      subMenuTween
        .to(itemToToggle.children[1], {
          height: "0",
          autoAlpha: 0,
          duration: 0.25,
        })
        .to(
          itemSiblings,
          {
            height: "auto",
            autoAlpha: 1,
            y: "0%",
            duration: 0.25,
            stagger: 0.05,
          },
          "+= .2"
        )
        .play();
    }
  };

  const handleRouteChangeStart = () => {
    document.body.style.overflow = "hidden";
    dispatch({ type: "TOGGLE_MENU", payload: { navOpen: false } });
  };

  const handleRouteChangeCompleted = () => {
    document.body.style.overflow = "auto";
  };

  return (
    <div
      className={`main-navigation ${navOpen ? "nav-open" : "nav-close"}`}
      ref={navRef}
    >
      <Logo />
      <ButtonCloseMenu />

      <div className="main-navigation__wrapper">
        <div className="main-navigation__inner" ref={wrapperInnerRef}>
          <div className="main-navigation__col main-navigation__col--left">
            <nav className="main-navigation__nav">
              <ul>
                {items.map(
                  (
                    { id, label, page, landing, slug, children }: any,
                    idx: number
                  ) => (
                    <Item
                      active={subMenuIdx === idx}
                      key={id}
                      idx={idx}
                      slug={slug}
                      link={page || landing}
                      label={label}
                      children={children}
                      onToggleSubMenu={_onToggleSubMenu}
                      ref={(el) => (menuList[idx] = el)}
                    />
                  )
                )}
              </ul>
            </nav>
          </div>

          <div
            className="main-navigation__col main-navigation__col--right"
            ref={navigationDetailsRef}
          >
            {widgets.map(({ id, title, content }) => {
              return (
                <div className="main-navigation__details" key={id}>
                  {title && <h4>{title}</h4>}
                  <div
                    dangerouslySetInnerHTML={{
                      __html: converter.makeHtml(content),
                    }}
                  />
                </div>
              );
            })}
          </div>
        </div>
        <div className="main-navigation__blinds" ref={blindsRef}>
          <div className="main-navigation__blind main-navigation__blind--left"></div>
          <div className="main-navigation__blind"></div>
          <div className="main-navigation__blind"></div>
          <div className="main-navigation__blind"></div>
          <div className="main-navigation__blind"></div>
          <div className="main-navigation__blind main-navigation__blind--right"></div>
        </div>
      </div>
    </div>
  );
};

export default withRouter(Nav);
