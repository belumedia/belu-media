import { forwardRef } from 'react';
import { slugify } from '../helpers/helper';

type Props = {
    template: string;
    title: string;
}

type Ref = HTMLSpanElement;

const Title = forwardRef<Ref,Props>(({ template, title }, pageTitle) => {
    return (
        <span className={`${template}__page-title ${ title ? slugify(title) : ''}`} ref={pageTitle}>{title}</span>
    );
});

export default Title;