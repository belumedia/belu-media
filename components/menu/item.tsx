import React, { forwardRef } from "react";
import Link from "next/link";

interface Props {
  idx: number;
  active: boolean;
  slug: string;
  link?: {
    slug: string;
    label: string;
  };
  label: string;
  children: [{ label: string; description: string; page: { slug: string }; landing: { slug: string } }];
  onToggleSubMenu: (idx: number) => void;
}

const Item = forwardRef<HTMLLIElement, Props>(
  (
    { link, label, slug, children, idx, active, onToggleSubMenu },
    ref
  ) => {
    const _onToggleSubMenu = (idx: number) => {
      onToggleSubMenu(idx);
    };

    const onMouseEnter = () =>
      document.body.classList.add("hover-main-navigation");
    const onMouseLeave = () =>
      document.body.classList.remove("hover-main-navigation");

    const buildPath = (parentslug: string, pageSlug: { slug: string }) => {
      if (parentslug) return `/${parentslug}/${pageSlug?.slug || "/"}`;
      return `/${pageSlug?.slug}`;
    };

    return (
      <li ref={ref} className={active ? "active" : null}>
          
        {link && (
          <Link href={buildPath(slug, link)}>
            <a onMouseEnter={onMouseEnter} onMouseLeave={onMouseLeave}>
              <span>{label}</span>
            </a>
          </Link>
        )}

        {!link && (
          <span
            onMouseEnter={onMouseEnter}
            onMouseLeave={onMouseLeave}
            onClick={() => _onToggleSubMenu(idx)}
          >
            {label}
          </span>
        )}

        {children.length > 0 && (
          <ul className="sub-menu">
            <li className="sub-menu__title">
              <span
                onMouseEnter={onMouseEnter}
                onMouseLeave={onMouseLeave}
                onClick={() => _onToggleSubMenu(idx)}
              >
                {label}
              </span>
            </li>

            {children.map(
              ({
                label,
                description,
                page,
                landing,
              }: {
                label: string;
                description: string;
                page: { slug: string };
                landing: { slug: string };
              }) => (
                <li key={label}>
                  <Link href={buildPath(slug, page ?? landing)}>
                    <a>
                      {label}
                      <span className="description">{description}</span>
                    </a>
                  </Link>
                </li>
              )
            )}
          </ul>
        )}
      </li>
    );
  }
);

export default Item;
