import React, { useState, useEffect, useRef } from 'react';
import { changeCursor, defaultCursor, toggleMenu } from 'state/action-creators';
import { useAppContext } from 'state/contexts/AppContext';

const ButtonOpenMenu = () => {

    const [disabled, setDisabled] = useState(false);
    const { navigation: { navOpen }, dispatch } = useAppContext();
    const btnMenuRef = useRef<HTMLButtonElement | null>(null);

    useEffect(() => {

        let btn = btnMenuRef.current;
        btn.addEventListener('mouseenter', onMouseEnter, true)
        btn.addEventListener('mouseleave', onMouseLeave, true)

        return () => {
            btn.removeEventListener('mouseenter', onMouseEnter)
            btn.removeEventListener('mouseleave', onMouseLeave)
        }

    },[])

    const onMouseEnter = () => {
        dispatch(changeCursor('cursor--hover-link'))
    }

    const onMouseLeave = () => {
        dispatch(defaultCursor)
    }

    const onClick = () => {
        disableMenu();
        dispatch(toggleMenu(!navOpen))
    }

    const disableMenu = () => {
        setDisabled(!disabled);
        setTimeout(() => {
            setDisabled(false)
        }, 1500)
    }

    return (
        <button
            className="menu-icon"
            type="button"
            disabled={disabled}
            onClick={onClick}
            ref={btnMenuRef}
            >
            <svg width="20" height="10" viewBox="0 0 20 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                <line y1="5" x2="20" y2="5" strokeWidth="2" />
                <line y1="1" x2="20" y2="1" strokeWidth="2" />
                <line y1="9" x2="20" y2="9" strokeWidth="2" />
            </svg>
            Menu
        </button>
    );
};

export default ButtonOpenMenu;