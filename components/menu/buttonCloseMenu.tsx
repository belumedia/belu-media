import React, { useState, useEffect, useRef } from 'react';
import { changeCursor, defaultCursor, toggleMenu } from 'state/action-creators';
import { useAppContext } from 'state/contexts/AppContext';


const ButtonCloseMenu = () => {

    const btnMenuRef = useRef<HTMLButtonElement | null>(null);
    const [disabled, setDisabled] = useState(false);
    const { dispatch } = useAppContext();

    useEffect(() => {

        const btnMenu = btnMenuRef.current;
        
        btnMenu.addEventListener('mouseenter', onMouseEnter, true)
        btnMenu.addEventListener('mouseleave', onMouseLeave, true)

        return () => {
            btnMenu.removeEventListener('mouseenter', onMouseEnter)
            btnMenu.removeEventListener('mouseleave', onMouseLeave)
        }

    }, [])


    const onMouseEnter = () => {
        dispatch(changeCursor('cursor--hover-link'))
    }

    const onMouseLeave = () => {
        dispatch(defaultCursor())
    }

    const onClick = () => {
        disableMenu();
        dispatch(toggleMenu(false))
    }

    const disableMenu = () => {
        setDisabled(!disabled);
        setTimeout(() => {
            setDisabled(false)
        }, 1500)
    }

    return (
        <button
            className="menu-icon"
            type="button"
            disabled={disabled}
            onClick={onClick}
            ref={btnMenuRef}>
            <svg width="16" height="16" viewBox="0 0 16 16">
                <line x1="1.35355" y1="0.646447" x2="15.4957" y2="14.7886" strokeWidth="2" />
                <line x1="0.646447" y1="14.7886" x2="14.7886" y2="0.646461" strokeWidth="2" />
            </svg>
            Close
        </button>
    );
};

export default ButtonCloseMenu;