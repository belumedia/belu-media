import { Converter } from 'showdown';

interface IProps {
    title: string;
    content: string;
}

const Widget = ({ title, content } : IProps) => {
    
    const converter = new Converter;

    return (
        <div className="page-footer__col">
            {title &&
                <h3 className="footer__title">{title}</h3>
            }
            <div className="footer__text" dangerouslySetInnerHTML={{ __html: converter.makeHtml(content) }} />
        </div>
    );
};

export default Widget;