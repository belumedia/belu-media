import Logo from './logo';
import ButtonOpenMenu from './menu/buttonOpenMenu';
import Socials from './socials';
import ScrollDown from './scrollDown';


const PageNavigation = () => {

    return (
        <>
            <Logo />
            <ButtonOpenMenu />
            <ScrollDown />
            <Socials />
        </>
    );
};

export default PageNavigation;