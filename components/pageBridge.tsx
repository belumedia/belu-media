import gsap from 'gsap';
import { useRouter } from 'next/router';
import React, { useEffect, useRef, useState } from 'react';

const PageBridge = () => {

    const router = useRouter();
    const pageBridgeRef = useRef<HTMLDivElement | null>(null);
    const [tweenBlinds] = useState(gsap.timeline({ paused: true }));

    useEffect(() => {

        const handleRouteStart = () => {
            pageBridgeRef.current.classList.add('page-bridge-enter-active')
            pageBridgeRef.current.classList.remove('page-bridge-leave-active')
        }

        const handleRouteComplete = () => {
            pageBridgeRef.current.classList.remove('page-bridge-enter-active')
            pageBridgeRef.current.classList.add('page-bridge-leave-active')
        }

        router.events.on('routeChangeStart', handleRouteStart)
        router.events.on('routeChangeComplete', handleRouteComplete)

        // If the component is unmounted, unsubscribe
        // from the event with the `off` method:
        return () => {
            router.events.off('routeChangeStart', handleRouteStart)
            router.events.off('routeChangeComplete', handleRouteComplete)
        }

    }, [])


    return (
        <div className="page-bridge page-bridge-leave-active" ref={pageBridgeRef} />
    );
};

export default PageBridge;