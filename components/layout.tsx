import Wrapper from './wrapper';
import PageNav from '../containers/pageNav';
import Cursors from './cursors';

type Props = {
    children: any
}

const Layout = ({ children } : Props) => {

    return (
        <>
            <PageNav />
            <Wrapper>
                {children}
            </Wrapper>
            <Cursors />
        </>
    );

}

export default Layout;