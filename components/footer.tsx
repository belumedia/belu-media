import { useEffect, useRef, useState } from "react";
import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import Widget from "./widget";
import Link from "next/link";

if (typeof window !== undefined) {
  gsap.registerPlugin(ScrollTrigger);
}

const Footer = ({ widgets, items }) => {
  const footerInnerRef = useRef<HTMLDivElement | null>(null);
  const footerTopRef = useRef<HTMLDivElement | null>(null);
  const footerBottonRef = useRef<HTMLDivElement | null>(null);
  const [footerTween] = useState(gsap.timeline({ paused: true }));

  useEffect(() => {
    footerTween
      .to([footerTopRef.current, footerBottonRef.current], {
        duration: 1,
        autoAlpha: 1,
        y: 0,
        stagger: {
          amount: 0.15,
        },
        scrollTrigger: {
          trigger: footerInnerRef.current,
          scrub: 1,
          start: "top bottom",
          end: "top bottom",
        },
      })
      .play();
  }, []);

  return (
    <div className="section-footer__inner" ref={footerInnerRef}>
      <div className="section-footer__top" ref={footerTopRef}>
        {widgets.map(({ id, title, content }) => (
          <Widget title={title} content={content} key={id} />
        ))}
      </div>

      <div className="section-footer__bottom" ref={footerBottonRef}>
        <ul>
          {items.map(({ id, label, page }: any, idx: number) => (
            <li key={id}>
              <Link href={page?.slug || "/"}>
                <a>
                  <span>{label}</span>
                </a>
              </Link>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default Footer;
