import gsap from 'gsap';
import { useRouter } from 'next/router';
import React, { useEffect, useRef, useState } from 'react';

const Blind = () => {

    const router = useRouter();
    const blindsRef = useRef<HTMLDivElement | null>(null);
    const [tweenBlinds] = useState(gsap.timeline({ paused: true }));

    useEffect(() => {

        const handleRouteStart = () => {

            tweenBlinds
                .set(blindsRef.current, {
                    autoAlpha: 1,
                })
                .set(blindsRef.current.children, {
                    height: '100%',
                    opacity: 1
                })
                .play()
        }

        const handleRouteComplete = () => {
            tweenBlinds
                .to(blindsRef.current.children, {
                    delay: .5,
                    duration: 0.5,
                    height: '0%',
                    ease: "power3.easeOut",
                    stagger: 0.07
                })
                .set(blindsRef.current, {
                    autoAlpha: 0
                }).play()
        }

        router.events.on('routeChangeStart', handleRouteStart)
        router.events.on('routeChangeComplete', handleRouteComplete)

        // If the component is unmounted, unsubscribe
        // from the event with the `off` method:
        return () => {
            router.events.off('routeChangeStart', handleRouteStart)
            router.events.off('routeChangeComplete', handleRouteComplete)
        }

    }, [])


    return (
        <div className="blinds" ref={blindsRef}>
            <div className="blind blind--left"></div>
            <div className="blind"></div>
            <div className="blind"></div>
            <div className="blind"></div>
            <div className="blind"></div>
            <div className="blind blind--right"></div>
        </div>
    );
};

export default Blind;