import React, { useEffect, useRef } from 'react';

declare global {
    interface Window {
        LandbotFrameWidget:any;
    }
}

const Landbot = () => {

    const landbotRef = useRef(null)

    useEffect(() => {
        
        if (typeof window.LandbotFrameWidget !== 'undefined') {

            const landbot = new window.LandbotFrameWidget({
                container: landbotRef.current,
                index: 'https://chats.landbot.io/v2/H-762479-LP7A32EV7YJNEUCW/index.html',
            })

            return () => landbot.destroy()
        }
        

    }, [])

    return <div className="landbot" ref={landbotRef}> </div>;
};

export default Landbot;