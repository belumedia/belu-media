// Gets a random integer.
const getRandomInt = (min: number, max: number) => Math.floor(Math.random() * (max - min + 1)) + min;

// Equation of a line (y = mx + b ).
const lineEq = (y2: number, y1: number, x2: number, x1: number, currentVal: number) => {
    const m = (y2 - y1) / (x2 - x1);
    const b = y1 - m * x1;
    return m * currentVal + b;
};

// Some random chars.
const chars = ['$', '%', '#', '&', '=', '*', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '.', ':', ',', '^'];
const charsTotal = chars.length;

// Randomize letters function. Used when navigating the slideshow to switch the curretn slide´s texts.
export const randomizeLetters = (letters: any[]) => {
    return new Promise((resolve, reject) => {
        const lettersTotal = letters.length;
        let cnt = 0;

        letters.forEach((letter: { innerHTML: string; style: { opacity: number; }; dataset: { initial: any; }; }, pos: number) => {
            let loopTimeout: NodeJS.Timeout;
            const loop = () => {
                letter.innerHTML = chars[getRandomInt(0, charsTotal - 1)];
                loopTimeout = setTimeout(loop, getRandomInt(50, 500));
            };
            loop();

            const timeout = setTimeout(() => {
                clearTimeout(loopTimeout);
                letter.style.opacity = 1;
                letter.innerHTML = letter.dataset.initial;
                ++cnt;
                if (cnt === lettersTotal) {
                    resolve(true);
                }
            }, pos * lineEq(40, 0, 8, 200, lettersTotal));
        });
    });
};


export const assembleLetters = (letters: any[]) => {
    return new Promise((resolve, reject) => {
        const lettersTotal = letters.length;
        let cnt = 0;

        letters.forEach((letter: { style: { opacity: number; }; }, pos: number) => {
            setTimeout(() => {
                letter.style.opacity = 1;
                ++cnt;
                if (cnt === lettersTotal) {
                    resolve(true);
                }
            }, pos * 30);
        });
    });
}

// Hide each of the letters with random delays. Used when showing the current slide´s content.
export const disassembleLetters = (letters: any[]) => {
    return new Promise((resolve, reject) => {
        const lettersTotal = letters.length;
        let cnt = 0;

        letters.forEach((letter: { style: { opacity: number; }; }, pos: number) => {
            setTimeout(() => {
                letter.style.opacity = 0;
                ++cnt;
                if (cnt === lettersTotal) {
                    resolve(true);
                }
            }, pos * 30);
        });
    });
}