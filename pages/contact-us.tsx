import { GetServerSideProps } from 'next';
import Head from '../components/head';
import Layout from "@components/layout";
import PageContent from '../components/pageContent';
import request from 'graphql-request';
import { queryPage } from 'graphql/queries';


interface IProps {
    data: {
        page: {
            title: string;
            description: string;
            template: string;
            sections: [{
                id: string;
                type: string;
                isScrollable: boolean;
            }]
        }
    }
}

const Contacts = ({ data: { page: { title, description, template, sections } } }: IProps) => {
    
 
    return (
        <Layout>
            <Head title={title} description={description} />
            <PageContent template={template} sections={sections} />
        </Layout>
    )
}


export const getServerSideProps: GetServerSideProps = async (ctx) => {

    const data = await request(process.env.NEXT_PUBLIC_API_GRAPHQL_HOST, queryPage, { slug: 'contact-us' });
    return { props: { data } }
}


export default Contacts;