import { GetServerSideProps } from "next";
import Error from "next/error";
import Head from "../components/head";
import Layout from "@components/layout";
import PageContent from "../components/pageContent";
import request from "graphql-request";
import { queryPage } from "graphql/queries";

interface IProps {
  data: {
    page: {
      title: string;
      description: string;
      template: string;
      sections: [
        {
          id: string;
          type: string;
          isScrollable: boolean;
        }
      ];
    };
  };
  statusCode: number;
}

const OurWork = ({ data: { page }, statusCode }: IProps) => {
  if (statusCode) {
    return <Error statusCode={statusCode} />;
  }

  return (
    <Layout>
      <Head title={page.title} description={page.description} />
      <PageContent template={page.template} sections={page.sections} />
    </Layout>
  );
};

export const getServerSideProps: GetServerSideProps = async ({ query }) => {
  const data = await request(
    process.env.NEXT_PUBLIC_API_GRAPHQL_HOST,
    queryPage,
    { slug: "our-work" }
  );
  const token = query.welcome_token;
  let statusCode: number | null = null;
  const errorCode =
    data.page && token === process.env.NEXT_PUBLIC_CASE_STUDIES_TOKEN
      ? false
      : true;
  if (errorCode) {
    statusCode = 404;
  }
  
  return { props: { data, statusCode } };
};

export default OurWork;
