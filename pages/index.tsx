import Head from '../components/head';
import PageContent from '../components/pageContent';
import request from 'graphql-request';
import { queryPage } from 'graphql/queries';
import Layout from '@components/layout';


interface IProps {
  data: {
    page: {
      title: string;
      description: string;
      template: string;
      sections: [{
        id: string;
        type: string;
        isScrollable: boolean;
      }]
    }
  }
}

const Home = ({ data: { page: { title, description, template, sections } } }: IProps) => {


  return (
    <Layout>
      <Head title={title} description={description} />
      <PageContent template={template} sections={sections}/>
    </Layout>
  )
}

export async function getServerSideProps() {

  const data = await request(process.env.NEXT_PUBLIC_API_GRAPHQL_HOST, queryPage, { slug: "" });
  return { props: { data } }

}

export default Home;