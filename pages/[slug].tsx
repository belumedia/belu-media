import { GetServerSideProps } from "next";
import Head from "@components/head";
import PageContent from "@components/pageContent";
import request from "graphql-request";
import Layout from "@components/layout";
import Error from "@components/errors"
import { queryPage } from "graphql/queries";

interface IProps {
  page: {
    title: string;
    description: string;
    template: string;
    sections: [
      {
        id: string;
        type: string;
        isScrollable: boolean;
      }
    ];
  };
  error: boolean;
}

const Page = ({ page, error }: IProps) => {

  if (error) {
    return <div className="page-error"><Error message="Page not found" backToHome={true} /></div>;
  }

  const { title, description, template, sections } = page;

  return (
    <Layout>
      <Head title={title} description={description} />
      <PageContent template={template} sections={sections} />
    </Layout>
  );
};

export const getServerSideProps: GetServerSideProps = async ({
  res,
  query,
}) => {
  const { slug } = query;

  const data = await request(
    process.env.NEXT_PUBLIC_API_GRAPHQL_HOST,
    queryPage,
    { slug }
  );

  const error = data.page ? false : true;

  return { props: { page: data.page, error } };
};

export default Page;
