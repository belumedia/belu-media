import Document, {
  Html,
  Head,
  Main,
  NextScript,
  DocumentContext,
} from "next/document";

class BeluMedia extends Document {
  static async getInitialProps(ctx: DocumentContext) {
    const initialProps = await Document.getInitialProps(ctx);
    return initialProps;
  }

  render() {
    return (
      <Html lang="en">
        <Head>

          <script
            dangerouslySetInnerHTML={{
              __html: `var _ss = _ss || [];
                  _ss.push(['_setDomain', 'https://koi-3QNSR8TWM8.marketingautomation.services/net']);
                  _ss.push(['_setAccount', 'KOI-4HXB747Q6G']);
                  _ss.push(['_trackPageView']);
                  window._pa = window._pa || {};
              (function() {
                  var ss = document.createElement('script');
                  ss.type = 'text/javascript'; ss.async = true;
                  ss.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'koi-3QNSR8TWM8.marketingautomation.services/client/ss.js?ver=2.4.0';
                  var scr = document.getElementsByTagName('script')[0];
                  scr.parentNode.insertBefore(ss, scr);
              })();`,
            }}
          />
          
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default BeluMedia;
