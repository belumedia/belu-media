import { AppProps } from "next/app";
import { PageTransition } from "next-page-transitions";
import { AppProvider } from "../state/contexts/AppContext";
import Preloader from "@components/preloader";
import "../styles.scss";

function BeluMedia({ Component, pageProps, router }: AppProps) {
  return (
    <PageTransition
      timeout={1000}
      classNames="page-transition"
      loadingComponent={<Preloader />}
      loadingDelay={2000}
      loadingClassNames="loading-indicator"
      loadingTimeout={{
        enter: 400,
        exit: 0,
      }}
    >
      <AppProvider>
        <Component {...pageProps} key={router.route} />
      </AppProvider>
    </PageTransition>
  );
}

export default BeluMedia;
