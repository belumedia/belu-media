import { GetServerSideProps } from "next";
import Head from "@components/head";
import request from "graphql-request";
import { queryLandingPages } from "graphql/queries";
import Layout from "@components/layout";
import PageContent from "@components/pageContent";

interface IProps {
  data: {
    landing: {
      title: string;
      description: string;
      template: string;
      slug: string;
      sections: [
        {
          id: string;
          type: string;
          isScrollable: boolean;
        }
      ];
    };
  };
}

const TopBusinessTech = ({
  data: {
    landing: { title, description, sections, slug },
  },
}: IProps) => {
  return (
    <Layout>
      <Head title={title} description={description} />
      <PageContent template="top-business-tech" sections={sections}/>
    </Layout>
  );
};

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const data = await request(
    process.env.NEXT_PUBLIC_API_GRAPHQL_HOST,
    queryLandingPages,
    { slug: "top-business-tech" }
  );
  return { props: { data } };
};

export default TopBusinessTech;
