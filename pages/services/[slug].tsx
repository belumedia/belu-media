import { GetServerSideProps } from 'next';
import Head from '@components/head';
import PageContent from '@components/pageContent';
import request from 'graphql-request';
import { queryPage, queryServices } from 'graphql/queries';
import Layout from '@components/layout';


interface IProps {
    data: {
        page: {
            title: string;
            description: string;
            template: string;
            sections: [{
                id: string;
                type: string;
                isScrollable: boolean;
            }]
        }
    }
}

const Page = ({ data: { page: { title, description, template, sections } } }: IProps) => {

    return (
        <Layout>
            <Head title={title} description={description} />
            <PageContent template={template} sections={sections} /> 
        </Layout>
    )
}


export const getServerSideProps: GetServerSideProps = async (ctx) => {
    const { slug } = ctx.query;
    const data = await request(process.env.NEXT_PUBLIC_API_GRAPHQL_HOST, queryPage, { slug });
    return { props: { data } }
}


export default Page;