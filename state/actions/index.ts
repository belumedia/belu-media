import { ActionType } from "../action-types";

export interface DefaultCursorAction {
  type: ActionType.DEFAULT_CURSOR;
}

export interface ChangeCursorAction {
  type: ActionType.CHANGE_CURSOR;
  payload: {
    style: string;
  };
}

export interface ToggleMenuAction {
  type: ActionType.TOGGLE_MENU;
  payload: {
    navOpen: boolean;
  };
}

export interface ToggleScrollAction {
  type: ActionType.TOGGLE_SCROLL;
  payload: {
    disableScroll: boolean;
  };
}

export interface ShowMemberInfoAction {
  type: ActionType.SHOW_MEMBER_INFO;
  payload: {
    index: number;
  };
}

export interface HideMemberInfoAction {
  type: ActionType.HIDE_MEMBER_INFO;
  payload: {
    index: number;
  };
}

export type Action =
  | DefaultCursorAction
  | ChangeCursorAction
  | ToggleMenuAction
  | ToggleScrollAction
  | ShowMemberInfoAction
  | HideMemberInfoAction;
