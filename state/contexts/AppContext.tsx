import { createContext, useContext, useReducer, Dispatch } from "react";
import { Action } from "state/actions";
import { appReducer, initialState, RootState } from "state/reducers";

export const AppContext = createContext(null);

export const AppProvider = ({ children }) => {
  const value = useAppProvider();
  return <AppContext.Provider value={value}> {children}</AppContext.Provider>;
};

export const useAppContext = () => {
  return useContext(AppContext);
};

const useAppProvider = () => {
  
  const [{ navigation, cursor, team }, dispatch] : [RootState, Dispatch<Action>] = useReducer(appReducer, initialState );

  return {
    navigation,
    cursor,
    team,
    dispatch,
  };
};
