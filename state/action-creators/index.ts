import { ActionType } from "../action-types";
import {
  DefaultCursorAction,
  ChangeCursorAction,
  HideMemberInfoAction,
  ShowMemberInfoAction,
  ToggleMenuAction,
  ToggleScrollAction,
} from "../actions";

export const toggleMenu = (navOpen: boolean): ToggleMenuAction => {
  return {
    type: ActionType.TOGGLE_MENU,
    payload: {
      navOpen,
    },
  };
};

export const toggleScroll = (disableScroll: boolean): ToggleScrollAction => {
  return {
    type: ActionType.TOGGLE_SCROLL,
    payload: {
      disableScroll,
    },
  };
};

export const showMemberInfo = (index: number): ShowMemberInfoAction => {
  return {
    type: ActionType.SHOW_MEMBER_INFO,
    payload: {
      index,
    },
  };
};

export const hideMemberInfo = (index: number): HideMemberInfoAction => {
  return {
    type: ActionType.HIDE_MEMBER_INFO,
    payload: {
      index,
    },
  };
};

export const changeCursor = (style: string): ChangeCursorAction => {
  return {
    type: ActionType.CHANGE_CURSOR,
    payload: {
      style,
    },
  };
};

export const defaultCursor = () : DefaultCursorAction => {
  return {
    type: ActionType.DEFAULT_CURSOR
  };
};
