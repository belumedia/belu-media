import combineReducers from 'react-combine-reducers';
import { reducer as navReducer, initialState as navInitialState } from 'state/reducers/nav.reducer';
import { reducer as cursorReducer, initialState as cursorInitialState } from 'state/reducers/cursor.reducer';
import { reducer as teamReducer, initialState as teamInitialState } from 'state/reducers/team.reducer';

export const [appReducer, initialState] = combineReducers({
    navigation: [navReducer, navInitialState],
    cursor: [cursorReducer, cursorInitialState],
    team: [teamReducer, teamInitialState]
});

export type RootState = ReturnType<typeof initialState>