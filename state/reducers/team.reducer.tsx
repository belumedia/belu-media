import { Action } from "state/actions";
import { ActionType } from "../action-types";

interface IMemberState {
  isHover: boolean;
  active: null | boolean;
}

export const initialState: IMemberState = {
  isHover: false,
  active: null,
};

export const reducer = (state : IMemberState = initialState, action: Action) => {
  switch (action.type) {
    case ActionType.SHOW_MEMBER_INFO:
      return {
        ...state,
        isHover: true,
        active: action.payload.index,
      };

    case ActionType.HIDE_MEMBER_INFO:
      return {
        ...state,
        isHover: false,
        active: action.payload.index,
      };
    default:
      return state;
  }
};
