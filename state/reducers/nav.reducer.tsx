import { Action } from "state/actions";
import { ActionType } from "../action-types";

interface INavState {
  navOpen: boolean;
  disableScroll: boolean;
}

export const initialState: INavState = {
  navOpen: false,
  disableScroll: false,
};

export const reducer = (state :INavState = initialState, action: Action) => {
  switch (action.type) {
    case ActionType.TOGGLE_MENU:
      return {
        ...state,
        navOpen: action.payload.navOpen,
      };

    case ActionType.TOGGLE_SCROLL:
      return {
        ...state,
        disableScroll: action.payload.disableScroll,
      };

    default:
      return state;
  }
};
