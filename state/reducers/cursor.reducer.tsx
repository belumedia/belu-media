import { Action } from "state/actions";
import { ActionType } from "../action-types";

interface ICursorState {
  style: string | null;
}

export const initialState : ICursorState = {
  style: null,
};

export const reducer = (
  state : ICursorState = initialState,
  action: Action
) => {
  switch (action.type) {
    case ActionType.DEFAULT_CURSOR:
      return {
        style: null,
      };

    case ActionType.CHANGE_CURSOR:
      return {
        ...state,
        style: action.payload.style,
      };

    default:
      return state;
  }
};
