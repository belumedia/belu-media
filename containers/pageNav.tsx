import { request } from "graphql-request";
import { queryMenu } from "../graphql/queries";
import Nav from "@components/nav";
import Loading from "@components/loading";
import useSWR from "swr";

interface IData {
  menu: {
    id: string;
    name: string;
    items: [
      {
        label: string;
        page?: {
          slug: string;
          title: string;
        };
        landing?: {
          slug: string;
          title: string;
        };
        children: [
          {
            label: string;
            description: string;
            page: {
              slug: string;
            };
          }
        ];
      }
    ];
  };
  widgets: {
    content: [
      {
        id: string;
        title: string;
        content: string;
      }
    ];
  };
}

const PageNav = () => {
  const { data, error } = useSWR(queryMenu, (query) =>
    request<IData>(process.env.NEXT_PUBLIC_API_GRAPHQL_HOST, query, {
      slug: "primary-menu",
    })
  );
  if (!data) return <Loading />;

  const { menu, widgets } = data;
  const { items } = menu ?? null;
  const { content } = widgets ?? null;

  return <Nav items={items} widgets={content} />;
};

export default PageNav;
