import { gql } from "graphql-request";

export const queryPages = gql`
  query Pages {
    pages {
      title
      name
      slug
    }
  }
`;

export const queryMenu = `
query MenuBySlug( $slug: String!) {
  menu: menuBySlug (slug : $slug) {
    id
   name
    items{
      id
      label
      slug
      page{
        title
        slug
      }
      children : submenu{
        id
        label
        description
        page{
          slug
        }
        landing{
          title
          slug
        }
      }
    }
  },
  widgets: widgetBySlug (slug : $slug){
    title
    content{
  		... on ComponentWidgetWidget {
        id
        title
        content
      }
    }
  }
}
`;

export const querySocials = gql`
  query WidgetBySlug($slug: String!) {
    widgets: widgetBySlug(slug: $slug) {
      title
      content {
        ... on ComponentWidgetWidget {
          id
          title
          content
        }
      }
    }
  }
`;

export const queryPage = gql`
  fragment BlockText on ComponentBlocksBlockText {
    _id
    title
    subtitle
    content
    target {
      id
      name
      title
      slug
    }
    longText
    color
    background
  }

  fragment BlockVideo on ComponentBlocksBlockVideo {
    _id
    media {
      url
      name
      mime
    }
    content
    color
    background
  }

  fragment BlockClients on ComponentBlocksBlockClients {
    id
    title
    subtitle
    items {
      id
      name
      url
      logo {
        id
        formats
        url
        mime
      }
    }
    color
    background
  }

  fragment BlockTeam on ComponentBlocksBlockTeam {
    id
    title
    content
    people {
      id
      name
      role
      description
      photo {
        url
        caption
        formats
      }
    }
    color
    background
  }

  fragment BlockLocations on ComponentBlocksBlockLocations {
    id
    title
    color
    background
    locations {
      id
      name
      phone
      email
      address
      mapUrl
      image {
        name
        url
        formats
      }
    }
  }

  fragment BlockTitle on ComponentBlocksBlockTitle {
    title
    subtitle
    content
    image {
      name
      url
      formats
    }
  }

  fragment BlockTransparency on ComponentAboutBlockTransparency {
    title
    content
    color
    background
  }

  fragment BlockLogo on ComponentBlocksBlockLogo {
    title
    content
    color
    background
  }

  fragment BlockQuote on ComponentBlocksBlockQuote {
    title
    subtitle
    color
    background
  }

  fragment BlockServicesIcons on ComponentBlocksBlockIcons {
    title
    items {
      id
      name
      icon {
        url
      }
    }
    services {
      id
      title
      path
    }
  }

  fragment BlockBelivies on ComponentAboutBlockWeBeliveIn {
    title
    subtitle
    color
    background
  }

  fragment BlockValues on ComponentAboutBlockValues {
    title
    link {
      text
      page {
        slug
      }
    }
    values {
      id
      title
      content
      icon
      background
      color
    }
    color
  }

  fragment BlockIconsItem on ComponentBlocksBlockIconsItems {
    items {
      id
      name
      icon {
        id
        url
      }
    }
  }

  fragment BlockImageText on ComponentBlocksBlockImageText {
    id
    title
    content
    image {
      id
      url
      formats
    }
  }

  fragment BlockIconsText on ComponentBlocksBlockTextIcons {
    id
    title
    content
    items {
      name
      description
      image {
        url
      }
    }
  }

  fragment BlockCaseStudies on ComponentBlocksBlockCaseStudies {
    items {
      id
      name
      campaign
      background
      preview {
        url
        width
        height
      }
      trackableLink
    }
  }

  
  query PageBySlug($slug: String!) {
    page: pageBySlug(slug: $slug) {
      _id
      slug
      name
      title
      description
      template
      sections {
        type: __typename
        ...BlockTitle
        ...BlockText
        ...BlockVideo
        ...BlockClients
        ...BlockTeam
        ...BlockLocations
        ...BlockTransparency
        ...BlockLogo
        ...BlockQuote
        ...BlockBelivies
        ...BlockValues
        ...BlockServicesIcons
        ...BlockIconsItem
        ...BlockImageText
        ...BlockIconsText
        ...BlockCaseStudies
      }
    }
  }
`;

export const queryFooter = gql`
  query WidgetBySlug($slug: String!) {
    widgets: widgetBySlug(slug: $slug) {
      title
      content {
        ... on ComponentWidgetWidget {
          id
          title
          content
        }
      }
    }
    menu: menuBySlug(slug: $slug) {
      id
      name
      items {
        id
        label
        page {
          title
          slug
        }
        children: submenu {
          label
          description
          page {
            slug
          }
        }
      }
    }
  }
`;

export const queryServices = gql`
  query {
    services {
      title
      items {
        id
        name
        icon {
          name
          url
        }
      }
      page {
        title
        slug
      }
    }
  }
`;

export const queryCulturePage = gql`
  query {
    page(id: "5cf148aafa5b70195860c5e4") {
      _id
      name
      title
      description
      template
      sections(where: { isActive: true }, sort: "order") {
        _id
        name
        title
        background
        color
        extraCssClass
        isActive
        blocks {
          _id
          name
          title
          content
          type
          target {
            _id
            title
            name
            pathname
          }
          media {
            id
            name
            url
            mime
          }
        }
      }
    }
    teams(sort: "order") {
      _id
      name
      role
      type
      description
      photo {
        id
        name
        url
      }
    }
  }
`;

export const queryServicesPage = gql`
  query {
    page(id: "5d1f52eb8740e45fc4489fca") {
      _id
      name
      title
      description
      template
      sections(where: { isActive: true }) {
        _id
        name
        background
        backgroundColor
        color
        extraCssClass
        isActive
        type
        blocks {
          name
          title
          subtitle
          content
          type
          target {
            _id
            title
            name
            pathname
          }
          media {
            id
            name
            url
            mime
          }
        }
      }
    }
    services {
      _id
      title
      text
      image {
        url
      }
    }
  }
`;

export const queryContactUsPage = gql`
  query {
    page(id: "5cf91f0f24a0ee322d50accf") {
      _id
      name
      title
      description
      template
      sections(where: { isActive: true }, sort: "order") {
        _id
        name
        title
        background
        color
        extraCssClass
        isActive
        blocks {
          _id
          name
          title
          content
          type
          target {
            _id
            title
            name
            pathname
          }
          media {
            id
            name
            url
            mime
          }
        }
      }
    }
    locations {
      name
      email
      address
      phone
      mapUrl
      image {
        id
        name
        url
        mime
      }
    }
  }
`;

export const querySingleCaseStudy = gql`
  query CaseStudy($id: ID!) {
    casestudy(id: $id) {
      _id
      customer
      introduction
      mainservice
      challenge
      results
      slug
      services
      preview {
        id
        name
        url
      }
      thumbnail {
        id
        name
        url
      }
      whattheysaid
      Entries {
        title
        description
        type
        extraCssClass
        media {
          id
          name
          url
        }
        alignment
      }
      Gallery {
        images {
          id
          name
          url
        }
      }
    }
  }
`;

export const queryCaseStudyBySlug = gql`
  query CaseStudyBySlug($slug: String!) {
    casestudies(where: { slug: $slug }) {
      _id
      customer
      introduction
      mainservice
      challenge
      results
      services
      slug
      year
      thumbnail {
        id
        name
        url
      }
      casestudyimages {
        description
        image {
          id
          name
          url
        }
        alignment
      }
    }
  }
`;

export const queryCaseStudies = gql`
  query {
    casestudies {
      _id
      customer
      introduction
      mainservice
      challenge
      results
      services
      slug
      thumbnail {
        id
        name
        url
      }
      casestudyimages {
        description
        image {
          id
          name
          url
        }
        alignment
      }
    }
  }
`;

export const queryPrivacyPage = gql`
  query {
    page(id: "5e468f160451be579a4c5c05") {
      _id
      name
      title
      description
      template
      sections(where: { isActive: true }, sort: "order") {
        _id
        name
        title
        background
        color
        extraCssClass
        isActive
        blocks {
          _id
          name
          title
          content
          type
          target {
            _id
            title
            name
            pathname
          }
          media {
            id
            name
            url
            mime
          }
        }
      }
    }
  }
`;

export const queryContactsWidgets = gql`
  query {
    widgets(where: { position: "contacts" }) {
      _id
      title
      content
      position
    }
  }
`;

export const queryLandingPages = gql`
  fragment BlockHeader on ComponentLandingBlockHeader {
    link
    logo {
      id
      url
      formats
    }
  }

  fragment BlockFooter on ComponentLandingBlockFooter {
    items {
      title
      content
    }
  }

  fragment BlockTitle on ComponentBlocksBlockTitle {
    title
    subtitle
    content
    image {
      name
      url
      formats
    }
  }


  fragment BlockImageText on ComponentBlocksBlockImageText {
    id
    title
    content
    image {
      id
      url
      formats
    }
  }

  fragment BlockIconsText on ComponentBlocksBlockTextIcons {
    id
    title
    content
    items {
      name
      description
      image {
        url
      }
    }
  }

  fragment BlockTextImage on ComponentBlocksBlockTextImage {
    id
    title
    content
    image {
      id
      url
      formats
    }
  }

  query LandingBySlug($slug: String!) {
    landing: landingBySlug(slug: $slug) {
      _id
      slug
      name
      title
      description
      sections {
        type: __typename
        ...BlockHeader
        ...BlockFooter
        ...BlockTitle
        ...BlockImageText
        ...BlockIconsText
        ...BlockTextImage
      }
    }
  }
`;
